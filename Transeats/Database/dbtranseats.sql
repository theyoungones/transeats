
DROP TABLE IF EXISTS tbdrivers;
CREATE TABLE IF NOT EXISTS tbdrivers(
	driverid int PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(50) NOT NULL,
	lastname VARCHAR(50) NOT NULL,
	birthdate date NOT NULL,
	sex text NOT NULL,
	dlicense VARCHAR(50) NULL
);

INSERT INTO tbdrivers ( firstname, lastname, birthdate, sex, dlicense ) values ( "Bus", "Driver1", "1983-04-12", "male", "license 1" );

INSERT INTO tbdrivers ( firstname, lastname, birthdate, sex, dlicense ) values ( "Bus", "Driver2", "1980-06-27", "male", "license 2" );

INSERT INTO tbdrivers ( firstname, lastname, birthdate, sex, dlicense ) values ( "Bus", "Driver3", "1983-01-02", "male", "license 3" );

INSERT INTO tbdrivers ( firstname, lastname, birthdate, sex, dlicense ) values ( "Bus", "Driver4", "1979-04-22", "male", "license 4" );

INSERT INTO tbdrivers ( firstname, lastname, birthdate, sex, dlicense ) values ( "Bus", "Driver5", "1988-10-10", "male", "license 5" );

DROP TABLE IF EXISTS tbconductors;
CREATE TABLE IF NOT EXISTS tbconductors(
	conductorid int PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(50) NOT NULL,
	lastname VARCHAR(50) NOT NULL,
	birthdate date NOT NULL,
	sex text NOT NULL
);

INSERT INTO tbconductors ( firstname, lastname, birthdate, sex ) values ( "Bus", "Conductor1", "1983-04-12", "male" );

INSERT INTO tbconductors ( firstname, lastname, birthdate, sex ) values ( "Bus", "Conductor2", "1980-06-27", "male" );

INSERT INTO tbconductors ( firstname, lastname, birthdate, sex ) values ( "Bus", "Conductor3", "1983-01-02", "male" );

INSERT INTO tbconductors ( firstname, lastname, birthdate, sex ) values ( "Bus", "Conductor4", "1979-04-22", "male" );

INSERT INTO tbconductors ( firstname, lastname, birthdate, sex ) values ( "Bus", "Conductor5", "1988-10-10", "male" );

DROP TABLE IF EXISTS tbbuses;
CREATE TABLE IF NOT EXISTS tbbuses(
	busno VARCHAR(50) NOT NULL PRIMARY KEY,
	driverid int NOT NULL,
	conductorid int NOT NULL,
	bustype int NOT NULL
);

DROP TABLE IF EXISTS tbbustypes;
CREATE TABLE IF NOT EXISTS tbbustypes(
	bustypeid int PRIMARY KEY AUTO_INCREMENT,
	bustypename VARCHAR(50) UNIQUE NOT NULL,
	airconditioned boolean NOT NULL,
	restroom boolean NOT NULL,
	capacity int NOT NULL
);

INSERT INTO tbbustypes ( bustypename, airconditioned, restroom, capacity ) values ( "Executive Coach 1", true, true, 26 );

INSERT INTO tbbustypes ( bustypename, airconditioned, restroom, capacity ) values ( "Executive Coach 2", true, true, 45 );

INSERT INTO tbbustypes ( bustypename, airconditioned, restroom, capacity ) values ( "Deluxe Coach", true, true, 55 );

DROP TABLE IF EXISTS tblocations;
CREATE TABLE IF NOT EXISTS tblocations(
	locationid int PRIMARY KEY AUTO_INCREMENT,
	locationname VARCHAR(50) NOT NULL UNIQUE,
	city VARCHAR(50) NOT NULL
);

INSERT INTO tblocations ( locationname, city ) values ( "JAC Terminal Balibago", "Sta. Rosa" );

INSERT INTO tblocations ( locationname, city ) values ( "JAC Terminal Buendia", "Makati" );

DROP TABLE IF EXISTS tbroutes;
CREATE TABLE IF NOT EXISTS tbroutes(
	routeid int PRIMARY KEY AUTO_INCREMENT,
	origin int NOT NULL,
	destination int NOT NULL,
	distance int NOT NULL,
	duration int NOT NULL,
	rate VARCHAR(10) NOT NULL
);

INSERT INTO tbroutes ( origin, destination, distance, duration, rate ) values ( 1, 2, 70, 60, 59 );

INSERT INTO tbroutes ( origin, destination, distance, duration, rate ) values ( 2, 1, 70, 60, 59 );

DROP TABLE IF EXISTS tbtrips;
CREATE TABLE IF NOT EXISTS tbtrips(
	tripid int PRIMARY KEY AUTO_INCREMENT,
	routeid int NOT NULL,
	busno VARCHAR(50) NOT NULL,
	tripdate date NOT NULL,
	departure time NOT NULL,
	availableseats int NOT NULL,
	tripstatus VARCHAR(10) DEFAULT "pending"
);

DROP TABLE IF EXISTS tbbusseats;
CREATE TABLE IF NOT EXISTS tbbusseats(
	seatid int PRIMARY KEY AUTO_INCREMENT,
	busno VARCHAR(50) NOT NULL,
	seatno VARCHAR(20) NULL,
	occupied boolean DEFAULT false
);

DROP TABLE IF EXISTS tbbookings;
CREATE TABLE IF NOT EXISTS tbbookings(
	bookingid int PRIMARY KEY AUTO_INCREMENT,
	tripid int NOT NULL,
	busno VARCHAR(50) NOT NULL,
	seatid int NOT NULL,
	accountid int NOT NULL
);

CREATE TABLE IF NOT EXISTS tbclientaccounts(
	accountid int PRIMARY KEY AUTO_INCREMENT,
	firstname VARCHAR(50) NOT NULL,
	lastname VARCHAR(50) NOT NULL,
	birthday date NOT NULL,
	mobileno bigint NOT NULL,
	email VARCHAR(50) NOT NULL,
	password VARCHAR(255),
	facebookid VARCHAR(50),
	photo VARCHAR(255)
);

DROP TABLE IF EXISTS tbmanagementaccounts;
CREATE TABLE IF NOT EXISTS tbmanagementaccounts(
	accountid int PRIMARY KEY AUTO_INCREMENT,
	username VARCHAR(50) NOT NULL,
	firstname VARCHAR(255) NOT NULL,
	lastname VARCHAR(255) NOT NULL,
	datecreated date NOT NULL,
	type text NOT NULL,
	password VARCHAR(255)
);

CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=theyoungones@localhost
	SQL SECURITY DEFINER VIEW viewbookings AS SELECT tbbookings.bookingid as bookingid, tbtrips.tripid as tripid, tbbuses.busno AS busno, tbbusseats.seatno AS seatno, CONCAT(tbclientaccounts.firstname, " ", tbclientaccounts.lastname) AS passenger FROM ((((tbbookings join tbtrips on ( tbbookings.tripid = tbtrips.tripid )) join tbbuses on ( tbbookings.busno = tbbuses.busno )) join tbclientaccounts on ( tbclientaccounts.accountid = tbbookings.accountid )) join tbbusseats on (tbbookings.seatid = tbbusseats.seatid));

CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=theyoungones@localhost
	SQL SECURITY DEFINER VIEW viewbusseats AS SELECT tbbusseats.seatid AS seatid, tbbuses.busno AS busno, tbbusseats.seatno AS seatno, tbbusseats.occupied AS occupied from (tbbusseats join tbbuses on (tbbusseats.busno = tbbuses.busno));

CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=theyoungones@localhost
	SQL SECURITY DEFINER VIEW viewbuses AS SELECT tbbuses.busno AS busno, CONCAT(tbdrivers.firstname, " ", tbdrivers.lastname) AS driver, CONCAT(tbconductors.firstname, " ", tbconductors.lastname) AS conductor, tbbustypes.bustypename AS bustype, tbbustypes.airconditioned AS airconditioned, tbbustypes.restroom AS restroom, tbbustypes.capacity as capacity from (tbbuses join tbdrivers on (tbbuses.driverid = tbdrivers.driverid) join tbconductors on (tbbuses.conductorid = tbconductors.conductorid) join tbbustypes on (tbbustypes.bustypeid = tbbuses.bustype));

CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=theyoungones@localhost
	SQL SECURITY DEFINER VIEW viewroutes AS SELECT a.routeid AS routeid, a.origin AS origin, b.destination AS destination, a.distance AS distance, a.duration AS duration, a.rate AS rate from (SELECT tbroutes.routeid AS routeid, CONCAT(tblocations.locationname, ", ", tblocations.city) AS origin, tbroutes.distance AS distance, tbroutes.duration AS duration, tbroutes.rate AS rate from (tbroutes join tblocations on (tbroutes.origin = tblocations.locationid))) as a JOIN (SELECT tbroutes.routeid AS routeid, CONCAT(tblocations.locationname, ", ", tblocations.city) AS destination from (tbroutes join tblocations on (tbroutes.destination = tblocations.locationid))) AS b on (a.routeid = b.routeid);

CREATE OR REPLACE ALGORITHM=UNDEFINED DEFINER=theyoungones@localhost
	SQL SECURITY DEFINER VIEW viewtrips AS SELECT tbtrips.tripid AS tripid, viewbuses.busno AS busno, viewbuses.driver AS driver, viewbuses.conductor AS conductor, viewroutes.origin AS origin, viewroutes.destination AS destination, tbtrips.tripdate as tripdate, tbtrips.departure AS departure, viewroutes.duration AS duration, viewroutes.rate AS rate, viewbuses.capacity AS capacity, tbtrips.availableseats AS availableseats, tbtrips.tripstatus as tripstatus from (tbtrips join viewbuses on (tbtrips.busno = viewbuses.busno) join viewroutes on (tbtrips.routeid = viewroutes.routeid));