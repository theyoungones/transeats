const mysql = require('mysql');
const http = require('http');
const StaticServer = require('node-static').Server;
const file = new StaticServer('./public');
const formidable = require('formidable');
const fs = require('fs');
const qs = require('querystring');
const conn = mysql.createConnection({
	user : process.env.SQL_USER,
	password : process.env.SQL_PASSWORD,
	database : process.env.SQL_DATABASE,
	socketPath : '/cloudsql/' + process.env.INSTANCE_CONNECTION_NAME
});
let handlers = {};
conn.connect(function(err) {
	if (err) {
		console.log("Failed to establish connection to mysql database" + err);
	}
	else {
		console.log("connected");
	}
});

handlers["/register"] = function(req, res) {
	if(req.method == "POST") {
		res.writeHead(200, {
			"Content-Type" : "application/json"
		});
		if(req.headers["content-type"].includes("multipart/form-data")) {
			let form = new formidable.IncomingForm();
			form.uploadDir="./";
			form.parse(req, (err, fields, files) => {
				fields.photo = fields.email + ".jpg"
				findFromDB("tbclientaccounts", {email : fields.email}, (obj) => {
					if(obj.error) {
						res.end(JSON.stringify(obj));
					}
					else if(obj.result.length > 0) {
						res.end(JSON.stringify({error: "Email address already taken"}));
					}
					else {
						conn.query("INSERT INTO tbclientaccounts SET ?", fields, (error, results, fields2) => {
							if(error) {
								res.end(JSON.stringify({error: error}))
							}
							else {
								let oldPath = files.file.path;
								let newPath = "./public/uploads/" + fields.photo;
								fs.access("./public/uploads", fs.constants.F_OK, (err) => {
									if(err) {
										fs.mkdir("./public/uploads", (err2) => {
											console.log(err2);
											renameFile(res, oldPath, newPath, fields);
										});
									}
									else {
										renameFile(res, oldPath, newPath, fields);
									}
								});
							}
						});
					}
				});
			});
		}
		else {
			extractData(req, (data) => {
				let user = JSON.parse(data);
				findFromDB("tbclientaccounts", {email : user.email}, (obj) => {
					if(obj.error) {
						res.end(JSON.stringify(obj));
					}
					else if(obj.result.length > 0) {
						res.end(JSON.stringify({error: "Email address already taken"}));
					}
					else {
						conn.query("INSERT INTO tbclientaccounts SET ?", user, (error, results, fields) => {
							if(error) {
								res.end(JSON.stringify({error: error}))
							}
							else {
								res.end(JSON.stringify({result: [user],
									task : "registration"}));
							}
						});
					}
				});
			});
		}
	}
};

handlers["/addmanagementaccount"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			let user = JSON.parse(data);
			findFromDB("tbmanagementaccounts", { username : user.username }, (obj) => {
				if(obj.error) {
					res.end(JSON.stringify({error : "Adding account failed."}));
				}
				else if(obj.result.length > 0) {
					res.end(JSON.stringify({ error: "Username already exists." }));
				}
				else {
					conn.query("INSERT INTO tbmanagementaccounts SET ?", user, (error, results, fields) => {
						if(error) {
							res.end(JSON.stringify({success: false}))
						}
						else {
							res.end(JSON.stringify({success: true}));
						}
					});
				}
			});
		});
	}
	else {
		returnBadAddress(res);
	}
};

handlers["/signin"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			let account = JSON.parse(data);
			findFromDB("tbclientaccounts", account, (obj) => {
				obj.task = "signin";
				res.end(JSON.stringify(obj));
			});
		});
	}
};

handlers["/account"] = (req, res) => {
	if(req.method == "GET") {
		if(req.url.includes("?")) {
			let qry = qs.parse(req.url);
			let qemail = qry["/account?email"];
			let id = qry["/account?id"];
			let task = qry["task"];
			let data = {};
			console.log(task);
			if(qemail) {
				data.email = qemail;
			}
			else if(id) {
				data.facebookid = id;
			}
			findFromDB("tbclientaccounts", data, (obj) => {
				obj.task = task;
				res.end(JSON.stringify(obj));
			});
		}
	}
};

handlers["/trips"] = (req, res) => {
	if(req.method == "GET") {
		if(req.url.includes("?")) {
			let qry = qs.parse(req.url);
			let orgn = qry["/trips?origin"].replace(/_/g, " ");
			let dstn = qry["destination"].replace(/_/g, " ");
			let dt = qry["date"];
			let trip = { origin : orgn, destination : dstn, tripdate : dt, status : "pending" };
			conn.query("SELECT * FROM viewtrips WHERE origin=? AND destination=? AND tripdate=? AND tripstatus=?", Object.values(trip), (error, results, fields) => {
				if(error) {
					res.end(JSON.stringify({error: error}))
				}
				else {
					res.end(JSON.stringify({result: results}));
				}
			});
		}
		else {
			conn.query("SELECT * FROM viewtrips ", (error, results, fields) => {
				if(error) {
					res.end(JSON.stringify({error: error}))
				}
				else {
					res.end(JSON.stringify({result: results}));
				}
			});
		}
	}
};

handlers["/seats"] = (req, res) => {
	if(req.method == "GET") {
		let qry = qs.parse(req.url);
		let bsnm = qry["/seats?busno"].replace(/_/g, " ");
		let seat = { busno : bsnm };
		conn.query("SELECT * FROM tbbusseats WHERE busno=?", Object.values(seat), (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}));
			}
			else {
				conn.query("SELECT bustype FROM viewbuses WHERE busno=?", Object.values(seat), (error, ress, fields) => {
					if(error) {
						res.end(JSON.stringify({error: error}));
					}
					else {
						res.end(JSON.stringify({result : { result : results, bustype : ress[0].bustype }}));
					}
				});
			}
		});
	}
};

handlers["/addterminal"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			if(data != "") {
				conn.query("INSERT INTO tblocations SET ?", JSON.parse(data), (error, results, fields) => {
					if(error) {
						console.log(error);
						res.end(JSON.stringify({ success : false }))
					}
					else {
						res.end(JSON.stringify({ success : true }));
					}
				});
			}
			else {
				res.end(JSON.stringify({ success : false }));
			}
		});
	}
	else {
		returnBadAddress(res);
	}
};

handlers["/addroute"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			if(data != "") {
				conn.query("INSERT INTO tbroutes SET ?", JSON.parse(data), (error, results, fields) => {
					if(error) {
						console.log(error);
						res.end(JSON.stringify({ success : false }))
					}
					else {
						res.end(JSON.stringify({ success : true }));
					}
				});
			}
			else {
				res.end(JSON.stringify({ success : false }));
			}
		});
	}
	else {
		returnBadAddress(res);
	}
};

handlers["/adddriver"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			if(data != "") {
				conn.query("INSERT INTO tbdrivers SET ?", JSON.parse(data), (error, results, fields) => {
					if(error) {
						console.log(error);
						res.end(JSON.stringify({ success : false }))
					}
					else {
						res.end(JSON.stringify({ success : true }));
					}
				});
			}
			else {
				res.end(JSON.stringify({ success : false }));
			}
		});
	}
	else {
		returnBadAddress(res);
	}
};

handlers["/addconductor"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			if(data != "") {
				conn.query("INSERT INTO tbconductors SET ?", JSON.parse(data), (error, results, fields) => {
					if(error) {
						console.log(error);
						res.end(JSON.stringify({ success : false }))
					}
					else {
						res.end(JSON.stringify({ success : true }));
					}
				});
			}
			else {
				res.end(JSON.stringify({ success : false }));
			}
		});
	}
	else {
		returnBadAddress(res);
	}
};

var bus = {};
	bus["1"] = {
		seats : [
			["A1", "B1", null, "C1"],
			["A2", "B2", null, "C2"],
			["A3", "B3", null, "C3"],
			["A4", "B4", null, null],
			["A5", "B5", null, null],
			["A6", "B6", null, "C6"],
			["A7", "B7", null, "C7"],
			["A8", "B8", null, "C8"],
			["A9", "B9", "D9", "C9"]
		],
		capacity : 26
	};
	bus["2"] = {
		seats : [
			["A1", "B1", null, "C1", "D1"],
			["A2", "B2", null, "C2", "D2"],
			["A3", "B3", null, "C3", "D3"],
			["A4", "B4", null, "C4", "D4"],
			["A5", "B5", null, "C5", "D5"],
			["A6", "B6", null, "C6", "D6"],
			["A7", "B7", null, "C7", "D7"],
			["A8", "B8", null, "C8", "D8"],
			["A9", "B9", null, "C9", "D9"],
			["A10", "B10", null, "C10", "D10"],
			["A11", "B11", "E11", "C11", "D11"]
		],
		capacity : 45
	};
	bus["3"] = {
		seats : [
			["A1", "B1", null, "C1", "D1"],
			["A2", "B2", null, "C2", "D2"],
			["A3", "B3", null, "C3", "D3"],
			["A4", "B4", null, "C4", "D4"],
			["A5", "B5", null, "C5", "D5"],
			["A6", "B6", null, "C6", "D6"],
			["A7", "B7", null, "C7", "D7"],
			["A8", "B8", null, "C8", "D8"],
			["A9", "B9", null, "C9", "D9"],
			["A10", "B10", null, "C10", "D10"],
			["A11", "B11", null, "C11", "D11"],
			["A12", "B12", null, "C12", "D12"],
			["A13", "B13", null, "C13", "D13"],
			["A14", "B14", "E14", "C14", "D14"]
		],
		capacity : 55
	};

handlers["/addbus"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			if(data != "") {
				let temp = JSON.parse(data);
				conn.query("INSERT INTO tbbuses SET ?", temp, (error, results, fields) => {
					if(error) {
						console.log(error);
						res.end(JSON.stringify({ success : false }))
					}
					else {
						for(let i = 0; i < bus[temp.bustype.toString()].seats.length; i++) {
							for(let j = 0; j < bus[temp.bustype.toString()].seats[i].length; j++) {
								let seat = { busno : temp.busno, seatno : bus[temp.bustype.toString()].seats[i][j] };
								conn.query("INSERT INTO tbbusseats SET ?", seat, (error, results, fields) => {
									if(error) {
										console.log(error);
										res.end(JSON.stringify({ success : false }));
										return;
									}
									else {
										if(i == bus[temp.bustype.toString()].seats.length - 1 && j == bus[temp.bustype.toString()].seats[i].length - 1) {
											res.end(JSON.stringify({ success : true }));
										}
									}
								});
							}
						}
					}
				});
			}
			else {
				res.end(JSON.stringify({ success : false }));
			}
		});
	}
	else {
		returnBadAddress(res);
	}
};

handlers["/addtrip"] = (req, res) => {
	if(req.method == "POST") {
		extractData(req, (data) => {
			if(data != "") {
				let tripp = JSON.parse(data);
				let a = { busno : tripp.busno };
				conn.query("SELECT bustype FROM tbbuses WHERE busno=?", Object.values(a), (error, results, fields) => {
					if(error) {
						res.end(JSON.stringify({error: error}));
					}
					else {
						let ind = results[0].bustype.toString();
						tripp.availableseats = bus[ind].capacity;
						conn.query("INSERT INTO tbtrips SET ?", tripp, (error, rrr, fields) => {
							if(error) {
								console.log(error);
								res.end(JSON.stringify({ success : false }))
							}
							else {
								res.end(JSON.stringify({ success : true }));
							}
						});
					}
				});
			}
			else {
				res.end(JSON.stringify({ success : false }));
			}
		});
	}
	else {
		returnBadAddress(res);
	}
};

handlers["/trip"] = (req, res) => {
	if(req.method == "GET") {
		let qry = qs.parse(req.url);
		let id = qry["/trips?id"];
		let trip = { tripid: id };
		conn.query("SELECT * FROM viewtrips WHERE tripid=?", Object.values(trip), (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}));
			}
			else {
				res.end(JSON.stringify({result: results}));
			}
		});
	}
};

handlers["/terminals"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT * FROM tblocations", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}));
			}
			else {
				res.end(JSON.stringify({result: results}));
			}
		});
	}
};

handlers["/buses"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT * FROM viewbuses", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}))
			}
			else {
				res.end(JSON.stringify({result: results}));
			}
		});
	}
};

handlers["/routes"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT * FROM viewroutes", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}))
			}
			else {
				res.end(JSON.stringify({result: results}));
			}
		});
	}
};

handlers["/accounts"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT accountid, username, firstname, lastname, datecreated, type FROM tbmanagementaccounts", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}))
			}
			else {
				res.end(JSON.stringify({result: results}));
			}
		});
	}
};

handlers["/bustypes"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT * FROM tbbustypes", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}))
			}
			else {
				res.end(JSON.stringify({result: results}));
			}
		});
	}
};

handlers["/locations"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT * FROM tblocations", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}))
			}
			else {
				console.log(results);
				res.end(JSON.stringify(results));
			}
		});
	}
};

handlers["/buscrews"] = (req, res) => {
	if(req.method == "GET") {
		conn.query("SELECT * FROM tbdrivers", (error, results, fields) => {
			if(error) {
				res.end(JSON.stringify({error: error}))
			}
			else {
				let temp = {};
				temp.type = "crews";
				temp.drivers = results;
				conn.query("SELECT * FROM tbconductors", (error, rr, fields) => {
					if(error) {
						res.end(JSON.stringify({error: error}))
					}
					else {
						temp.conductors = rr;
						for(driver of temp.drivers) {
							driver.type = "Driver";
						}
						for(cond of temp.conductors) {
							cond.dlicense = "N/A";
							cond.type = "Conductor";
						}
						res.end(JSON.stringify({result: temp}));
					}
				});
			}
		});
	}
};


handlers["/"] = (req, res) => {
	file.serve(req, res, (err) => {
		if(err && err.status == 404) {
			returnBadAddress(res);
		}
	});
};

var server = http.createServer(function(req, res) {
	console.log(req.url);
	let url = (req.url.includes("?")) ? req.url.split("?")[0] : req.url;
	console.log(url);
	if(handlers[url]) {
		handlers[url](req, res);
	}
	else {
		handlers["/"](req, res);
	}
});

server.listen(8080);
console.log("Server is running ..");

let renameFile = (res, oldPath, newPath, user) => {
	fs.rename(oldPath, newPath, (err) => {
		if(err) {
			res.end(JSON.stringify({error: err}))
		}
		else {
			res.end(JSON.stringify({result : [user]}));
		}
	});
};

let extractData = (req, callback) => {
	let data = "";
	req.on('data', (chunk) => {
		console.log("Extracting data..");
		data += chunk;
	});
	req.on('end', () => {
		console.log("Extracted: " + data);
		callback(data);
	});
};

let findFromDB = (table, obj, callback) => {
	let sqlQuery = "SELECT * FROM " + conn.escapeId(table);
	let inserts = [];
	for(let i = 0; obj && i < Object.keys(obj).length; i++) {
		sqlQuery += (i == 0) ? " WHERE ?? = ?" : " AND ?? = ?";
		inserts.push(Object.keys(obj)[i]);
		inserts.push(Object.values(obj)[i]);
	}
	let queryString = mysql.format(sqlQuery + ";", inserts);
	console.log("Query: " + queryString);
	conn.query(queryString, (err, results, fields) => {
		if(err) {
			console.log("DB query error: " + err);
			callback({error: err});
		}
		else {
			console.log("DB query results: " + results);
			callback({result: results});
		}
	});
};

let returnBadAddress = (res) => {
	res.writeHead(404, { "Content-Type" : "text/html" });
	res.end("Error: Page Not Found");
};
