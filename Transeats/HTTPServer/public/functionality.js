window.onload = () => {
	let tableholder = document.getElementById("maincontent");
	let mterminals = document.getElementById("terminals");
	let mbuses = document.getElementById("buses");
	let mbuscrews = document.getElementById("buscrews");
	let mbustrips = document.getElementById("bustrips");
	let mroutes = document.getElementById("routes");
	let maccounts = document.getElementById("accounts");
	mterminals.onclick = () => {
		console.log("Pinindot");
		highlightMenu(mterminals);
		removeAllChild(tableholder);
		request("GET", "/terminals", null, (reply) => {
			let headers = ["ID", "Terminal Name", "City"]
			createTable(reply.result, headers, "Terminal");
		});
	};
	mbuses.onclick = () => {
		console.log("Pinindot");
		highlightMenu(mbuses);
		removeAllChild(tableholder);
		request("GET", "/buses", null, (reply) => {
			let headers = ["Bus No.", "Driver", "Conductor", "Bus Type", "Airconditioned", "Restroom", "Capacity"]
			createTable(reply.result, headers, "Bus");
		});
	};
	mbuscrews.onclick = () => {
		console.log("Pinindot");
		highlightMenu(mbuscrews);
		removeAllChild(tableholder);
		request("GET", "/buscrews", null, (reply) => {
			let headers = ["Firstname", "Lastname", "Birthdate", "Sex", "Driver's License", "Crew Type"]
			createTable(reply.result, headers, "Buscrew");
		});
	};
	mbustrips.onclick = () => {
		console.log("Pinindot");
		highlightMenu(mbustrips);
		removeAllChild(tableholder);
		request("GET", "/trips", null, (reply) => {
			let headers = ["Trip ID", "Bus No.", "Driver", "Conductor", "Origin", "Destination", "Trip Date", "Departure", "Travel Duration", "Fare", "Capacity", "Available Seats", "Trip Status"];
			createTable(reply.result, headers, "Bustrip");
		});
	};
	mroutes.onclick = () => {
		console.log("Pinindot");
		highlightMenu(mroutes);
		removeAllChild(tableholder);
		request("GET", "/routes", null, (reply) => {
			let headers = ["Route ID", "Origin", "Destination", "Distance", "Duration", "Fare"];
			createTable(reply.result, headers, "Route");
		});
	};
	maccounts.onclick = () => {
		console.log("Pinindot");
		highlightMenu(maccounts);
		removeAllChild(tableholder);
		request("GET", "/accounts", null, (reply) => {
			let headers = ["ID", "Username", "Firstname", "Lastname", "Date Created", "Type"];
			createTable(reply.result, headers, "Account");
		});
	};
}

function highlightMenu(menuitem) {
	let a = document.getElementsByClassName("selected");
	if(a[0]) {
		a[0].className = "menu";
	}
	menuitem.className = "selected";
};

function createElement(parentid, type, id, classname) {
	let parent = document.getElementById(parentid);
	let element = document.createElement(type);
	if(id) {
		element.id = id;
	}
	if(classname) {
		element.className = classname;
	}
	parent.appendChild(element);
	return(element);
}

function removeAllChild(parent) {
	parent.innerHTML = "";
}

function createTable(objs, headers, flag) {
	let con = document.getElementById("maincontent");
	let divv = document.createElement("div");
	divv.id = "hldr";
	t = document.createElement('table');
	t.id = "table";
	let header = document.createElement('tr');
	fillHeader(header, headers);
	t.appendChild(header);
	if(objs.type == "crews") {
		for(obj of objs.drivers) {
			let tr = document.createElement('tr');
			for(key in obj) {
				if(key != "driverid") {
					let td = document.createElement('td');
					td.appendChild(document.createTextNode(obj[key]));
					tr.appendChild(td);
				}
			}
			t.appendChild(tr);
		}
		for(obj of objs.conductors) {
			let tr = document.createElement('tr');
			for(key in obj) {
				if(key != "conductorid") {
					let td = document.createElement('td');
					td.appendChild(document.createTextNode(obj[key]));
					tr.appendChild(td);
				}
			}
			t.appendChild(tr);
		}
	}
	else {
		for(obj of objs) {
			let tr = document.createElement('tr');
			for(key in obj) {
				let td = document.createElement('td');
				td.appendChild(document.createTextNode(obj[key]));
					tr.appendChild(td);
			}
			t.appendChild(tr);
		}
	}
	con.appendChild(divv);
	divv.appendChild(t);
	let addbutton = createElement("hldr", "button", "add" + flag.toLowerCase(), "addbuttons");
	addbutton.innerHTML = "Add " + flag;
	addbutton.onclick = () => {
		let iw = createPopup("New " + flag);
		let url = null;
		let inputs = {};
		switch(flag) {
			case "Terminal":
				inputs.name = createElement("inputscontainer", "input", null, null);
				inputs.name.placeholder = "Terminal Name"
				inputs.city = createElement("inputscontainer", "input", null, null);
				inputs.city.placeholder = "City"
				break;
			case "Route":
				inputs.origin = createElement("inputscontainer", "select", "origin", null);
				createElement("origin", "option", null, null).appendChild(document.createTextNode("Select Origin"));
				request("GET", "/locations", null, function(objects) {
					let locs = objects;
					if(locs) {
						console.log("nareceive na");
						inputs.origin.onfocus = function() {
							inputs.origin.innerHTML = "";
							for(let object of locs) {
								let a = createElement("origin", "option", null, null);
								a.appendChild(document.createTextNode(object.locationname + ", " + object.city));
								a.value = object.locationid;
							}
						}
						inputs.destination.onfocus = function() {
							inputs.destination.innerHTML = "";
							for(let object of locs) {
								let a = createElement("destination", "option", null, null);
								a.appendChild(document.createTextNode(object.locationname + ", " + object.city));
								a.value = object.locationid;
							}
						}
					}
				});
				inputs.destination = createElement("inputscontainer", "select", "destination", null);
				createElement("destination", "option", null, null).appendChild(document.createTextNode("Select Destination"));
				inputs.distance = createElement("inputscontainer", "input", "distance", null);
				inputs.distance.type = "number";
				inputs.distance.placeholder = "Distance (km)";
				inputs.duration = createElement("inputscontainer", "input", "duration", null);
				inputs.duration.type = "number";
				inputs.duration.placeholder = "Travel Duration (minutes)";
				inputs.rate = createElement("inputscontainer", "input", "duration", null);
				inputs.rate.type = "number";
				inputs.rate.placeholder = "Trip Price";
				break;
			case "Buscrew":
				inputs.lname = createElement("inputscontainer", "input", null, null);
				inputs.lname.placeholder = "Last Name"
				inputs.fname = createElement("inputscontainer", "input", null, null);
				inputs.fname.placeholder = "First Name"
				inputs.sex = createElement("inputscontainer", "select", "sex", null);
				createElement("sex", "option", null, null).appendChild(document.createTextNode("Male"));
				createElement("sex", "option", null, null).appendChild(document.createTextNode("Female"));
				createElement("inputscontainer", "p", null, null).appendChild(document.createTextNode("Birthdate:"));
				inputs.birthdate = createElement("inputscontainer", "input", null, null);
				inputs.birthdate.type = "date";
				inputs.birthdate.value = "1980-01-01";
				createElement("inputscontainer", "p", null, null).appendChild(document.createTextNode("Crew Type:"));
				inputs.ctype = createElement("inputscontainer", "select", "ctype", null);
				createElement("ctype", "option", null, null).appendChild(document.createTextNode("Driver"));
				createElement("ctype", "option", null, null).appendChild(document.createTextNode("Conductor"));
				inputs.license = createElement("inputscontainer", "input", null, null);
				inputs.license.placeholder = "Driver's License";
				prevstate = "Driver"
				inputs.ctype.onchange = function() {
					if(inputs.ctype.value == "Driver") {
						inputs.license.disabled = false;
					}
					else if(ctype.value == "Conductor") {
						inputs.license.disabled = true;
					}
					prevstate = inputs.ctype.value;
				}
				break;
			case "Bus":
				inputs.busno = createElement("inputscontainer", "input", "busno", null);
				inputs.busno.placeholder = "Bus No.";
				inputs.driver = createElement("inputscontainer", "select", "driver", null);
				createElement("driver", "option", null, null).appendChild(document.createTextNode("Select driver"));
				request("GET", "/buscrews", null, function(objects) {
					let crewsss = objects.result;
					if(crewsss) {
						inputs.driver.onfocus = function() {
							inputs.driver.innerHTML = "";
							for(let object of crewsss.drivers) {
								let a = createElement("driver", "option", null, null);
								a.appendChild(document.createTextNode(object.firstname + " " + object.lastname));
								a.value = object.driverid;
							}
						}
						inputs.conductor.onfocus = function() {
							inputs.conductor.innerHTML = "";
							for(let object of crewsss.conductors) {
								let a = createElement("conductor", "option", null, null);
								a.appendChild(document.createTextNode(object.firstname + " " + object.lastname));
								a.value = object.conductorid;
							}
						}
					}
				});
				inputs.conductor = createElement("inputscontainer", "select", "conductor", null);
				createElement("conductor", "option", null, null).appendChild(document.createTextNode("Select conductor"));
				inputs.bustype = createElement("inputscontainer", "select", "bustype", null);
				createElement("bustype", "option", null, null).appendChild(document.createTextNode("Select Type"));
				request("GET", "/bustypes", null, function(objects) {
					let bustypes = objects.result
					if(bustypes) {
						inputs.bustype.onfocus = function() {
							bustype.innerHTML = "";
							for(let object of bustypes) {
								let a = createElement("bustype", "option", null, null);
								a.appendChild(document.createTextNode(object.bustypename));
								a.value = object.bustypeid;
							}
						}
					}
				});
				break;
			case "Bustrip":
				inputs.routeid = createElement("inputscontainer", "select", "routeid", null);
				createElement("routeid", "option", null, null).appendChild(document.createTextNode("Select Route"));
				inputs.busno = createElement("inputscontainer", "select", "busid", null);
				createElement("busid", "option", null, null).appendChild(document.createTextNode("Select bus"));
				request("GET", "/routes", null, function(objects) {
					let rts = objects.result;
					if(rts.length > 0) {
						inputs.routeid.onfocus = function() {
							inputs.routeid.innerHTML = "";
							for(let object of rts) {
								let a = createElement("routeid", "option", null, null);
								a.appendChild(document.createTextNode(object.origin + " - " + object.destination));
								a.value = object.routeid;
							}
						}
					}
					else {
						alert("There is no registered route yet.");
					}
				});
				request("GET", "/buses", null, function(objects) {
					let bss = objects.result;
					if(bss.length) {
						inputs.busno.onfocus = function() {
							inputs.busno.innerHTML = "";
							for(let object of bss) {
								let a = createElement("busid", "option", null, null);
								a.appendChild(document.createTextNode(object.busno));
								a.value = object.busno;
							}
						}
					}
					else {
						alert("There is no registered bus yet.");
						cancel.click();
					}
				});
				createElement("inputscontainer", "p", null, null).appendChild(document.createTextNode("Trip date:"));
				inputs.tripdate = createElement("inputscontainer", "input", null, null);
				inputs.tripdate.type = "date";
				let today = new Date();
				let dd = today.getDate();
				let mm = today.getMonth() + 1;
				let yyyy = today.getFullYear();
				inputs.tripdate.value = yyyy + "-" + ((mm > 9) ? "" : "0") + mm + "-" + ((dd > 9) ? "" : "0") + dd;
				createElement("inputscontainer", "p", null, null).appendChild(document.createTextNode("Departure time:"));
				inputs.departure = createElement("inputscontainer", "input", null, null);
				inputs.departure.type = "time";
				let hh = today.getHours();
				let m = today.getMinutes();
				inputs.departure.value = ((hh > 9) ? "" : "0") + hh + ":" + ((m > 9) ? "" : "0") + m;
				break;
			case "Account":
				inputs.username = createElement("inputscontainer", "input", null, null);
				inputs.username.placeholder = "Username";
				inputs.firstname = createElement("inputscontainer", "input", null, null);
				inputs.firstname.placeholder = "Firstname";
				inputs.lastname = createElement("inputscontainer", "input", null, null);
				inputs.lastname.placeholder = "Lastname";
				inputs.type = createElement("inputscontainer", "select", "accounttype", null);
				createElement("accounttype", "option", null, null).appendChild(document.createTextNode("Regular"));
				createElement("accounttype", "option", null, null).appendChild(document.createTextNode("Admin"));
				break;
		}
		let confirm = createElement("inputscontainer", "button", null, "popupbuttons");
		confirm.appendChild(document.createTextNode("Confirm"));
		let cancel = createElement("inputscontainer", "button", null, "popupbuttons");
		cancel.appendChild(document.createTextNode("Cancel"));
		let ipw = document.getElementById("inputpanelwrapper");
		confirm.onclick = function() {
			let flag2 = true;
			for(key in inputs) {
				if(inputs[key].value == "" || inputs[key].value.includes("Select ")) {
					if(key != "license") {
						flag2 = false;
					}
					else {
						if(inputs.ctype.value == "Driver") {
							flag2 = false;
						}
					}
				}
			}
			if(flag2 == false) {
				alert("Please don't leave any empty field.");
				return;
			}
			let body = {};
			switch(flag) {
				case "Terminal":
					body.locationname = inputs.name.value;
					body.city = inputs.city.value;
					url = "/addterminal";
					break;
				case "Route":
					if(inputs.origin.value == inputs.destination.value) {
						alert("Origin and destination must be different terminals");
						return;
					}
					url = "/addroute";
					body.origin = inputs.origin.value;
					body.destination = inputs.destination.value;
					body.distance = inputs.distance.value;
					body.duration = inputs.duration.value;
					body.rate = inputs.rate.value;
					break;
				case "Buscrew":
					body.firstname = inputs.fname.value;
					body.lastname = inputs.lname.value;
					body.birthdate = inputs.birthdate.value;
					body.sex = inputs.sex.value;
					if(inputs.ctype.value == "Driver") {
						body.dlicense = inputs.license.value;
						url = "/adddriver";
					}
					else {
						url = "/addconductor";
					}
					break;
				case "Bus":
					url = "/addbus";
					body.busno = inputs.busno.value;
					body.driverid = inputs.driver.value;
					body.conductorid = inputs.conductor.value;
					body.bustype = inputs.bustype.value;
					break;
				case "Bustrip":
					url = "/addtrip";
					body.routeid = inputs.routeid.value;
					body.busno = inputs.busno.value;
					body.tripdate = inputs.tripdate.value;
					body.departure = inputs.departure.value;
					break;
				case "Account":
					url = "/addmanagementaccount";
					body.username = inputs.username.value;
					body.firstname = inputs.firstname.value;
					body.lastname = inputs.lastname.value;
					body.type = inputs.type.value;
					let today = new Date();
					let dd = today.getDate();
					let mm = today.getMonth() + 1;
					let yyyy = today.getFullYear();
					body.datecreated = yyyy + "-" + ((mm > 9) ? "" : "0") + mm + "-" + ((dd > 9) ? "" : "0") + dd;
					break;
			}
			if(url != null) {
				request("POST", url, body, function(response) {
					iw.removeChild(document.getElementById("inputscontainer"));
					ipw.className = "hidden";
					document.getElementById(flag.toLowerCase() + ((flag == "Bus") ? "es" : "s")).click();
					if(response.success == true) {
						alert(flag + " was successfully added.");
					}
					else {
						alert(flag + " was not added successfully.");
					}
				});
			}
		}
		cancel.onclick = function() {
			iw.removeChild(document.getElementById("inputscontainer"));
			ipw.className = "hidden";
		}
		ipw.className = "shown";
	}
}

let createPopup = (headerString) => {
	let a = document.getElementById("inputswrapper");
	createElement("inputswrapper", "div", "inputscontainer", null);
	document.getElementById("popuplabel").innerHTML = headerString;
	return a;
}

let request = (method, url, obj, callback) => {
	let rq = new XMLHttpRequest();
	rq.onreadystatechange = function() {
		if(rq.readyState == 4) {
			console.log("response url: " + rq.responseURL);
			if(callback) {
				console.log(rq.responseText);
				callback(JSON.parse(rq.responseText));
			}
		}
	};
	rq.open(method, url, true);
	if(obj != null) {
		rq.send(JSON.stringify(obj));
	}
	else {
		rq.send();
	}
}

function fillHeader(header, obj) {
	for(key of obj) {
		let th = document.createElement('th');
		th.appendChild(document.createTextNode(key));
		header.appendChild(th);
	}
}
