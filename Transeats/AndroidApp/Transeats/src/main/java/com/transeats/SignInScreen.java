package com.transeats;

import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.InputType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.PopupWindow;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;
import com.facebook.login.LoginManager;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import java.util.List;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.Arrays;
import org.json.JSONObject;

public class SignInScreen extends Activity
{
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	ScrollView scrollView = null;
	TranseatsTextFieldMaker textFieldMaker = null;
	TranseatsLabelMaker labelMaker = null;
	TranseatsDropDownMaker dropdownMaker = null;
	TranseatsButtonMaker buttonMaker = null;
	TranseatsPopupWindowMaker popupwindowmaker = null;
	ImageView ibLogo = null;
	EditText tEmail = null;
	EditText tPassword = null;
	TextView lnForgotPassword = null;
	Button bSignIn = null;
	TextView lOr = null;
	Button bSignInWithFacebook = null;
	Button bContinueAsGuest = null;
	TextView lDontHaveAccount = null;
	TextView lnSignUpHere = null;
	CallbackManager callbackManager = null;
	PopupWindow popup = null;
	Activity thisAct = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		thisAct = (Activity)this;
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		textFieldMaker = new TranseatsTextFieldMaker(this);
		labelMaker = new TranseatsLabelMaker(this);
		buttonMaker = new TranseatsButtonMaker(this);
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(this);
		popup = popupwindowmaker.getPopup();
		LayoutParams p1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		p1.gravity = Gravity.CENTER;
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setLayoutParams(p1);
		contentLayout = new LinearLayout(this);
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		mainLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		final LoginManager loginManager = LoginManager.getInstance();
		callbackManager = CallbackManager.Factory.create();
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout.setPadding((int)(screenWidth * 0.1), (int)(screenHeight * 0.07), (int)(screenWidth * 0.1), 0);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(p1);
		scrollView.setScrollbarFadingEnabled(false);
		scrollView.setVerticalScrollBarEnabled(true);
		scrollView.setVerticalFadingEdgeEnabled(false);
		ibLogo = new ImageView(this);
		ibLogo.setImageResource(R.drawable.transeats);
		LayoutParams ibparams = new LayoutParams((int)(screenWidth * 0.5), (int)(screenWidth * 0.5));
		ibparams.gravity = Gravity.CENTER;
		ibparams.setMargins(0, 0, 0, (int)(screenHeight * 0.05));
		ibLogo.setLayoutParams(ibparams);
		contentLayout.addView(ibLogo);
		tEmail = textFieldMaker.createTextField("Email");
		contentLayout.addView(tEmail);
		tPassword = textFieldMaker.createTextField("Password");
		tPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
		contentLayout.addView(tPassword);
		lnForgotPassword = labelMaker.createLabel("Forgot Password?", Color.BLUE, Typeface.BOLD);
		LayoutParams lnFPParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lnFPParams.gravity = Gravity.CENTER;
		lnFPParams.setMargins(0, (int)(screenHeight * 0.01), 0, (int)(screenHeight * 0.01));
		lnForgotPassword.setLayoutParams(lnFPParams);
		lnForgotPassword.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				System.out.println("Forgot Password clicked");
				String email = tEmail.getText().toString();
				if(email.equals("")) {
					popupwindowmaker.showPopupWindow("Please enter an Email Address.", contentLayout);
				}
				else if(!email.matches("\\S+@\\S+\\.\\S+")) {
					popupwindowmaker.showPopupWindow("Invalid email format.", contentLayout);
				}
				else {
					String url = TranseatsIP.IP + "/account?email=" + email + "&task=forgotPassword";
					new GetTask(SignInScreen.this, url, new AccountsProcessor(SignInScreen.this, contentLayout, null), popupwindowmaker, contentLayout).execute();
				}
			}
		}, lnForgotPassword, popup));
		contentLayout.addView(lnForgotPassword);
		bSignIn = buttonMaker.createButton("Sign In", Color.rgb(252, 179, 24), (float)0.8, Gravity.CENTER);
		bSignIn.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				if(!popup.isShowing()) {
					String[][] fields = {{"email", "password"},
						{tEmail.getText().toString(), tPassword.getText().toString()}
					};
					if(fields[1][0].equals("") || fields[1][1].equals("")) {
						popupwindowmaker.showPopupWindow("Please complete all required fields to sign in.", contentLayout);
					}
					else if(!fields[1][0].matches("\\S+@\\S+\\.\\S+")) {
						popupwindowmaker.showPopupWindow("Invalid email format.", contentLayout);
					}
					else {
						new PostTask(SignInScreen.this, TranseatsIP.IP + "/signin", new AccountsProcessor(SignInScreen.this, contentLayout, null), contentLayout).execute(fields);
					}
				}
			}
		}, bSignIn, popup));
		contentLayout.addView(bSignIn);
		lOr = labelMaker.createLabel("OR", Color.rgb(100, 100, 100), -1);
		LayoutParams lOrParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lOrParams.gravity = Gravity.CENTER;
		lOrParams.setMargins(0, (int)(screenHeight * 0.01), 0, (int)(screenHeight * 0.01));
		lOr.setLayoutParams(lOrParams);
		contentLayout.addView(lOr);
		bSignInWithFacebook = buttonMaker.createButton("Sign In with Facebook", Color.rgb(59, 89, 152), (float)0.8, Gravity.CENTER);
		Drawable fbIcon = this.getResources().getDrawable(R.drawable.fb);
		bSignInWithFacebook.setCompoundDrawablesWithIntrinsicBounds(fbIcon, null, null, null);
		bSignInWithFacebook.setPadding(30, 0, 0, 0);
		loginManager.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
			@Override
			public void onSuccess(LoginResult loginresult) {
				AccessToken accessToken = loginresult.getAccessToken();
				Bundle parameters = new Bundle();
				parameters.putString("fields", "id,birthday,email,first_name,last_name,picture.height(800).width(800)");
				new GraphRequest(
					accessToken, "/" + accessToken.getUserId(), parameters, HttpMethod.GET,
					new GraphRequest.Callback() {
						public void onCompleted(GraphResponse response) {
							try {
								JSONObject obj = response.getJSONObject();
								System.out.println(obj.toString());
								if(obj.has("id") && !"".equals(obj.getString("id"))) {
									String url = TranseatsIP.IP + "/account?id=" + obj.getString("id") + "&task=facebook";
									new GetTask(SignInScreen.this, url, new AccountsProcessor(SignInScreen.this, contentLayout, obj.toString()), popupwindowmaker, contentLayout).execute();
								}
								else {
									Intent intent = new Intent(SignInScreen.this, RegistrationScreen.class);
									intent.putExtra("facebookDetails", obj.toString());
									startActivity(intent);
								}
							}
							catch(Exception e) {
								e.printStackTrace();
							}
						}
					}
				).executeAsync();
			}
			@Override
			public void onCancel() {
				System.out.println("Login attempt cancelled");
			}

			@Override
			public void onError(FacebookException e) {
				System.out.println("Login attempt failed.");
			}
		});
		bSignInWithFacebook.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				if(ConnectionChecker.isConnected(getApplicationContext())) {
					loginManager.logInWithReadPermissions(SignInScreen.this,
						Arrays.asList(new String[]{"email", "user_birthday"}));
				}
				else {
					popupwindowmaker.showPopupWindow("Please check your internet connection and try again.", contentLayout);
				}
			}
		}, bSignInWithFacebook, popup));
		contentLayout.addView(bSignInWithFacebook);
		bContinueAsGuest = buttonMaker.createButton("Continue as Guest", Color.rgb(140, 140, 140), (float)0.8, Gravity.CENTER);
		Drawable guestIcon = this.getResources().getDrawable(R.drawable.guest);
		bContinueAsGuest.setCompoundDrawablesWithIntrinsicBounds(guestIcon, null, null, null);
		bContinueAsGuest.setPadding(30, 0, 0, 0);
		contentLayout.addView(bContinueAsGuest);
		LinearLayout accountLayout = new LinearLayout(this);
		LayoutParams p2 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		p2.gravity = Gravity.CENTER;
		p2.setMargins(0, (int)(screenHeight * 0.02), 0, 0);
		accountLayout.setLayoutParams(p2);
		lDontHaveAccount = labelMaker.createLabel("Don't have an Account yet? ", Color.rgb(60, 60, 60), -1);
		accountLayout.addView(lDontHaveAccount);
		lnSignUpHere = labelMaker.createLabel("Sign Up here", Color.BLUE, Typeface.BOLD);
		accountLayout.addView(lnSignUpHere);
		contentLayout.addView(accountLayout);
		scrollView.addView(contentLayout);
		mainLayout.addView(scrollView);
		lnSignUpHere.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(SignInScreen.this, RegistrationScreen.class));
			}
		}, lnSignUpHere, popup));
		bContinueAsGuest.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startActivity(new Intent(SignInScreen.this, FindTrips.class));
				thisAct.finish();
			}
		}, bContinueAsGuest, popup));
		setContentView(mainLayout);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		callbackManager.onActivityResult(requestCode, resultCode, data);
	}
}
