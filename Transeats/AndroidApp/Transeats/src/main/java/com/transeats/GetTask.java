package com.transeats;

import java.net.HttpURLConnection;
import java.net.URL;
import android.os.AsyncTask;
import java.lang.StringBuilder;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.Spinner;
import android.view.Gravity;
import org.json.JSONObject;
import org.json.JSONArray;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import java.io.FileInputStream;
import java.io.DataOutputStream;
import android.net.Uri;
import android.content.ContentResolver;
import android.content.Intent;
import java.util.ArrayList;
import java.util.List;

public class GetTask extends AsyncTask<String, Void, String>
{
	TranseatsPopupWindowMaker popupwindowmaker;
	Context context;
	URL url;
	View view;
	List<?> object = null;
	JSONProcessor jsonprocessor;
	LinearLayout layout = null;
	LinearLayout viewHolder = null;
	JSONObject fbDetails = null;
	PopupWindow popup = null;

	public GetTask(Context c, String u, View view, JSONProcessor jsonprocessor, TranseatsPopupWindowMaker popupwindowmaker, LinearLayout layout) {
		try {
			url = new URL(u);
			context = c;
			this.layout = layout;
			this.jsonprocessor = jsonprocessor;
			this.popupwindowmaker = popupwindowmaker;
			this.view = view;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public GetTask(Context c, String u, View view, JSONProcessor jsonprocessor, TranseatsPopupWindowMaker popupwindowmaker, LinearLayout layout, List<?> object) {
		try {
			url = new URL(u);
			context = c;
			this.layout = layout;
			this.jsonprocessor = jsonprocessor;
			this.popupwindowmaker = popupwindowmaker;
			this.view = view;
			this.object = object;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public GetTask(Context c, String u, JSONProcessor jsonprocessor, TranseatsPopupWindowMaker popupwindowmaker, LinearLayout layout) {
		try {
			url = new URL(u);
			System.out.println("URL: " + url.toString());
			context = c;
			this.layout = layout;
			this.jsonprocessor = jsonprocessor;
			this.popupwindowmaker = popupwindowmaker;
			this.view = null;
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	protected String doInBackground(String[] str) {
		StringBuilder sb = new StringBuilder("");
		try {
			if(ConnectionChecker.isConnected(context)) {
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				InputStream in = urlConnection.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String line = "";
				while((line = br.readLine()) != null) {
					sb.append(line);
				}
				urlConnection.disconnect();
			}
			else {
				sb.append("{error : \"Please check your internet connection and try again.\"}");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			return("{error : \"Please check your internet connection and try again.\"}");
		}
		return(sb.toString());
	}

	protected void onPostExecute(String result) {
		try {
			JSONObject obj = new JSONObject(result);
			if(obj.has("error")) {
				popupwindowmaker.showPopupWindow(obj.getString("error"), layout);
			}
			else if(obj.has("result")) {
				Object res = obj.get("result");
				if(res != null) {
					jsonprocessor.process(res, view, object);
				}
			}
			else {
				popupwindowmaker.showPopupWindow("Please check your internet connection and try again.", layout);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
