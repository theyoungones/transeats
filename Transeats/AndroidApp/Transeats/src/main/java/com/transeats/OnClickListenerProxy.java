package com.transeats;

import android.view.View;
import android.widget.PopupWindow;

public class OnClickListenerProxy implements View.OnClickListener
{
	private View view;
	private PopupWindow popup;
	private View.OnClickListener listener;

	public OnClickListenerProxy(View.OnClickListener l, View v, PopupWindow p) {
		listener = l;
		view = v;
		popup = p;
	}

	public void onClick(View v) {
		if(!popup.isShowing()) {
			listener.onClick(v);
		}
	}
}
