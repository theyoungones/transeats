package com.transeats;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.graphics.Color;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;

public class PrivacyPolicyScreen extends Activity
{
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	TextView tContent = null;
	ScrollView scrollView = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsLabelMaker labelMaker = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setBackgroundColor(Color.WHITE);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		scrollView.setBackgroundColor(Color.rgb(195, 195, 195));
		scrollView.setScrollbarFadingEnabled(false);
		scrollView.setVerticalScrollBarEnabled(true);
		scrollView.setVerticalFadingEdgeEnabled(false);
		contentLayout = new LinearLayout(this);
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
		p1.gravity = Gravity.CENTER;
		contentLayout.setPadding(50, 20, 50, 20);
		contentLayout.setLayoutParams(p1);
		labelMaker = new TranseatsLabelMaker(this);
		tContent = labelMaker.createLabel("Transeats is committed to respecting your privacy and protecting your personal information. We have written this privacy policy to ensure you of how we use the information you have entrusted us with. This policy describes the information we collect and how it is used.\n\nPersonal Information We Collect\n\nPersonal Information is information about you whether it was provided by you directly, such as when you create your account, or it was collected through your use of the app, such as usage and device information.\n\nTranseats collects the following categories of information:\n\n1. Information you provide\n\nBefore being able to use our booking services, you are required to make an account where you will provide the following pieces of information:\n\n \t• First Name\n \t• Last Name\n \t• Birthday\n \t• E-mail Address\n \t• Contact Number\n \t• Photo\n \nIn the case of registration through Facebook, the same information will be collected from Facebook.\n\n2. Information created when you use our services\n\nBooking Information:\n\n \t• When you use our booking services, we collect the travel date, travel origin and travel destination, amount charged, number of seats booked and other related booking details.\n\n Device Information\n\n \t• We may collect information about the devices you use to access our services including operating system and version, software, file names, unique device identifier, and mobile network information.\n\nHow We Use Your Information\n\nTranseats collects information in order to provide customers our reliable and efficient booking services.\n\n We may use personal information to:\n\n \t• Validate your identity in regards to booking transactions and payments.\n \t• Add personalization to your Transeats account.\n \t• Communicate with you for announcements.\n \t• Enhance the services and features of Transeats.\n\nRetention and Deletion of Personal Information\n\nPersonal information shall be retained as long as the account remains active. When you delete your account and we no longer need your personal information we will delete all copies of your information.\n\nUpdates to This Policy\n\nWe may update this privacy policy occasionally and you will be informed through the Transeats app in case that happens. By continuing to use the Transeats app’s services, you are agreeing to the updates made to this policy.\n", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(tContent);
		headerMaker = new TranseatsHeaderMaker(this, "PRIVACY POLICY", false);
		scrollView.addView(contentLayout);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(scrollView);
		setContentView(mainLayout);
	}
}
