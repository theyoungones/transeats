package com.transeats;

import android.widget.LinearLayout;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Button;
import android.widget.PopupMenu;
import android.view.View;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.view.Gravity;
import android.util.DisplayMetrics;
import android.graphics.Typeface;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import java.io.FileInputStream;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;

public class TranseatsHeaderMaker
{
	Context con = null;
	String headerString = null;
	boolean hasMenu = false;
	RelativeLayout layout = null;
	int screenWidth = 0;
	int screenHeight = 0;
	String[][] account;

	public TranseatsHeaderMaker(Context con, String headerString, boolean hasMenu) {
		this.con = con;
		this.headerString = headerString;
		this.hasMenu = hasMenu;
		account = null;
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
		setHeaderLayout();
	}

	public TranseatsHeaderMaker(Context con, String headerString, String[][] a) {
		this.con = con;
		this.headerString = headerString;
		this.hasMenu = (a != null && a.length > 0) ? true : false;
		account = a;
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
		setHeaderLayout();
	}

	public void setHeaderLayout() {
		layout = new RelativeLayout(con);
		layout.setBackgroundColor(Color.rgb(252, 179, 24));
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenHeight = displayMetrics.heightPixels;
		Button but = new Button(con);
		but.setBackground(((Activity)con).getResources().getDrawable(R.drawable.back));
		LayoutParams layoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, (int)(screenHeight * 0.09));
		LayoutParams butParams = new LayoutParams((int)(screenHeight * 0.04), (int)(screenHeight * 0.04));
		butParams.addRule(RelativeLayout.ALIGN_PARENT_LEFT);
		butParams.addRule(RelativeLayout.CENTER_VERTICAL);
		layout.setLayoutParams(layoutParams);
		but.setLayoutParams(butParams);
		layout.addView(but);
		layout.setPadding((int)(screenWidth * 0.02), 0, (int)(screenWidth * 0.04), 0);
		int layoutWidth = layout.getWidth();
		TextView tv = new TextView(con);
		tv.setTextSize(20);
		tv.setTextColor(Color.rgb(255, 255, 255));
		tv.setText(headerString);
		tv.setTypeface(null, Typeface.BOLD);
		LayoutParams tvParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		tvParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tv.setLayoutParams(tvParams);
		layout.addView(tv);
		if(hasMenu == true) {
			Drawable icon = ((Activity)con).getResources().getDrawable(R.drawable.menu);
			Button menu = new Button(con);
			/* if(account != null) {
				for(String[] row : account) {
					try {
						if(row.length > 1 && row[0] != null && row[0].equals("photo") && row[1] != null && !row[1].equals("")) {
							FileInputStream fis = con.openFileInput(row[1]);
							Bitmap img = BitmapFactory.decodeStream(fis);
							icon = new BitmapDrawable(((Activity)con).getResources(), img);
							break;
						}
					}
					catch(Exception e) {
						e.printStackTrace();
					}
				}
			} */
			menu.setBackground(icon);
			LayoutParams menuParams = new LayoutParams((int)(screenHeight * 0.04), (int)(screenHeight * 0.04));
			menuParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
			menuParams.addRule(RelativeLayout.CENTER_VERTICAL);
			menu.setLayoutParams(menuParams);
			layout.addView(menu);
			menu.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					PopupMenu pmenu = new PopupMenu(con, v);
					pmenu.getMenu().add("Profile");
					pmenu.getMenu().add("Transactions");
					pmenu.getMenu().add("Sign Out");
					pmenu.show();
					pmenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
						@Override
						public boolean onMenuItemClick(MenuItem menuItem) {
							switch(menuItem.getTitle().toString()){
								case "Profile":
									if(!"ProfileScreen".equals(con.getClass().getSimpleName())) {
										((Activity)con).startActivity(new Intent(con.getApplicationContext(), ProfileScreen.class));
									}
									return true;
								case "Transactions":
									if(!"TransactionsScreen".equals(con.getClass().getSimpleName())) {
										((Activity)con).startActivity(new Intent(con.getApplicationContext(), TransactionsScreen.class));
									}
									return true;
								case "Sign Out":
									AccountsDataSource.getInstance(con).deleteAccounts();
									android.widget.Toast.makeText(con, "Sign Out", android.widget.Toast.LENGTH_SHORT).show();
									return true;
								default:
									return false;
							}
						}
					});
				}
			});
		}
		but.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				((Activity)con).finish();
			}
		});
	}

	public RelativeLayout getHeader() {
		return(layout);
	}
}
