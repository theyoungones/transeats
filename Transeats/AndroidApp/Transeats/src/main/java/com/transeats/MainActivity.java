package com.transeats;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.Button;
import android.view.View;
import android.graphics.Color;
import android.content.Intent;
import android.view.Window;

public class MainActivity extends Activity
{
	LinearLayout mainLayout = null;
	Button lSignUp = null;
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		lSignUp = new Button(this);
		lSignUp.setText("Sign Up");
		mainLayout.addView(lSignUp);
		lSignUp.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), SplashScreen.class));
			}
		});
		setContentView(mainLayout);
	}
}
