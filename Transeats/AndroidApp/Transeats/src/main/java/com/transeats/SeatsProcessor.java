package com.transeats;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONArray;
import android.view.View;
import android.view.Gravity;
import android.content.Context;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ArrayAdapter;
import android.graphics.Color;
import java.time.LocalDate;
import java.time.LocalTime;

public class SeatsProcessor implements JSONProcessor
{
	Context con = null;
	int screenWidth = 0;
	int screenHeight = 0;

	public SeatsProcessor(Context con, int screenWidth, int screeHeight) {
		this.con = con;
		this.screenWidth = screenWidth;
		this.screenHeight = screenHeight;
	}

	public void process(Object res, View view, List<?> obj) {
		try {
			JSONObject job = (JSONObject)res;
			Object seatss = job.get("result");
			String bustype = job.getString("bustype");
			if(seatss instanceof JSONArray) {
				JSONArray array = (JSONArray)seatss;
				ArrayList<BusSeat> list = (ArrayList<BusSeat>)obj;
				int cnt = -1;
				TranseatsBoxMaker boxMaker = new TranseatsBoxMaker(con);
				RelativeLayout layout = (RelativeLayout)view;
				LinearLayout row = new LinearLayout(con);
				LinearLayout holder = new LinearLayout(con);
				holder.setOrientation(LinearLayout.VERTICAL);
				RelativeLayout.LayoutParams hParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
				hParams.addRule(RelativeLayout.CENTER_IN_PARENT);
				hParams.addRule(RelativeLayout.CENTER_VERTICAL);
				holder.setLayoutParams(hParams);
				layout.addView(holder);
				row.setOrientation(LinearLayout.HORIZONTAL);
				holder.addView(row);
				int lim = ("Executive Coach 1".equals(bustype)) ? 4 : 5;
				for(int i = 0; i < array.length(); i++) {
					JSONObject jsonObj = array.getJSONObject(i);
					BusSeat seat = new BusSeat(jsonObj.getInt("seatid"), jsonObj.getString("busno"), ("null".equals(jsonObj.getString("seatno")) ? null : jsonObj.getString("seatno")), (jsonObj.getInt("occupied") == 0) ? false : true);
					list.add(seat);
					cnt++;
					if(cnt >= lim) {
						cnt = -1;
						i -= 1;
						row = new LinearLayout(con);
						row.setOrientation(LinearLayout.HORIZONTAL);
						holder.addView(row);
					}
					else {
						int color = Color.WHITE;
						int borderColor = Color.rgb(140, 140, 140);
						String txt = seat.getSeatNo();
						if(txt == null) {
							borderColor = Color.WHITE;
							color = Color.WHITE;
						}
						if(seat.occupied()) {
							color = Color.RED;
						}
						RelativeLayout box = boxMaker.createSeat((int)(screenWidth * 0.08), (int)(screenWidth * 0.08), borderColor, color, (txt == null) ? "" : txt);
						row.addView(box);
					}
				}
				android.widget.Toast.makeText(con, String.valueOf(list.get(3).getSeatNo()), android.widget.Toast.LENGTH_SHORT).show();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
