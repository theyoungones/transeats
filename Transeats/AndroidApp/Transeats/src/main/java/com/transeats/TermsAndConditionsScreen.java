package com.transeats;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.graphics.Color;
import android.view.ViewGroup.LayoutParams;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;

public class TermsAndConditionsScreen extends Activity
{
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	TextView tContent = null;
	ScrollView scrollView = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsLabelMaker labelMaker = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		mainLayout.setBackgroundColor(Color.WHITE);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		scrollView.setBackgroundColor(Color.rgb(195, 195, 195));
		scrollView.setScrollbarFadingEnabled(false);
		scrollView.setVerticalScrollBarEnabled(true);
		scrollView.setVerticalFadingEdgeEnabled(false);
		contentLayout = new LinearLayout(this);
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 0, 1);
		p1.gravity = Gravity.CENTER;
		contentLayout.setPadding(50, 20, 50, 20);
		contentLayout.setLayoutParams(p1);
		labelMaker = new TranseatsLabelMaker(this);
		tContent = labelMaker.createLabel("Transeats is an online bus booking platform application. It does not operate bus services of its own. \n\n Transeats responsibilities include:\n\n \t• Issuing a valid reservation book seat printed or screenshot and surrender to the authorized officer of the bus company in the terminal address indicated in the transaction reference number, in exchange for a ticket that will be issued by authorized officer upon their verification of your reservation.\n\nTranseats responsibilities do NOT include:\n\n \t• Any injury or damage to you and/or your properties.\n\n \t• The bus company not departing / reaching on time.\n\n \t• The baggage of the customer getting lost / stolen / damaged.\n\n \t• The company shall not be responsible for any commercial and/or personal losses due to delay or cancellation of schedule trip because of unforeseen circumstances.\n\n \t• In case of force majeure or unavoidable circumstances, the management reserves the right to cancel the trip and/or change the date and time of departure without prior notice.\n", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(tContent);
		headerMaker = new TranseatsHeaderMaker(this, "TERMS AND CONDITIONS", false);
		scrollView.addView(contentLayout);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(scrollView);
		setContentView(mainLayout);
	}
}
