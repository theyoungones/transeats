package com.transeats;

import android.app.Activity;
import android.database.Cursor;
import android.os.Bundle;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.Manifest;
import android.net.Uri;
import android.os.Parcelable;
import android.graphics.drawable.GradientDrawable;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.content.ContextCompat;
import android.support.v4.app.ActivityCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.InputFilter;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.Window;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.CheckBox;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.CompoundButton;
import java.util.List;
import java.util.Calendar;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.HashMap;
import java.io.File;
import org.json.JSONObject;


public class RegistrationScreen extends Activity
{
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	LinearLayout bdayHolder = null;
	ScrollView scrollView = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsTextFieldMaker textFieldMaker = null;
	TranseatsLabelMaker labelMaker = null;
	TranseatsDropDownMaker dropdownMaker = null;
	TranseatsButtonMaker buttonMaker = null;
	TranseatsPopupWindowMaker popupwindowmaker = null;
	CheckBox cb = null;
	ImageView ibUploadPhoto = null;
	TextView lTermsAndConditions = null;
	TextView lPrivacyPolicy = null;
	TextView lUploadPhoto = null;
	TextView lAnd = null;
	TextView lFirstName = null;
	TextView lLastName = null;
	TextView lBirthday = null;
	TextView lPassword = null;
	EditText tPassword = null;
	TextView lConfirmPassword = null;
	EditText tConfirmPassword = null;
	TextView lMobileNumberPrefix = null;
	TextView lMobileNumber = null;
	EditText tMobileNumber = null;
	EditText tEmail = null;
	TextView lEmail = null;
	EditText tFirstName = null;
	EditText tLastName = null;
	Spinner ddMonth = null;
	Spinner ddDay = null;
	Spinner ddYear = null;
	Button bSignUp = null;
	Button bCancel = null;
	List<String> months = null;
	List<String> days = null;
	List<String> years = null;
	LinkedHashMap<String, String> monthsMap = null;
	LinkedHashMap<String, EditText> textFields = null;
	Uri imageUri = null;
	GradientDrawable shape = null;
	PopupWindow popup = null;
	JSONObject fbDetails = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		textFieldMaker = new TranseatsTextFieldMaker(this);
		labelMaker = new TranseatsLabelMaker(this);
		dropdownMaker = new TranseatsDropDownMaker(this);
		buttonMaker = new TranseatsButtonMaker(this);
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(this);
		popup = popupwindowmaker.getPopup();
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout = new LinearLayout(this);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		scrollView.setScrollbarFadingEnabled(false);
		scrollView.setVerticalScrollBarEnabled(true);
		scrollView.setVerticalFadingEdgeEnabled(false);
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		mainLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		LayoutParams p1 = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
		p1.gravity = Gravity.CENTER;
		contentLayout.setLayoutParams(p1);
		contentLayout.setPadding(30, 5, 30, 5);
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		final Boolean fromFacebook = this.getIntent().hasExtra("facebookDetails");
		textFields = new LinkedHashMap<String, EditText>();
		ibUploadPhoto = new ImageView(this);
		ibUploadPhoto.setImageResource(R.drawable.uploadphoto);
		ibUploadPhoto.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				openChooser();
			}
		}, ibUploadPhoto, popup));
		lUploadPhoto = new TextView(this);
		lUploadPhoto.setText("Upload Photo");
		lUploadPhoto.setTextColor(Color.rgb(0, 0, 0));
		lUploadPhoto.setTypeface(null, Typeface.BOLD);
		lFirstName = labelMaker.createLabel("First Name", Color.rgb(0, 0, 0), Typeface.BOLD);
		tFirstName = textFieldMaker.createTextField("First Name");
		tFirstName.setSingleLine(true);
		tFirstName.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		InputFilter[] filter = new InputFilter[]{new InputFilter.LengthFilter(50)};
		tFirstName.setFilters(filter);
		textFields.put("firstname", tFirstName);
		lLastName = labelMaker.createLabel("Last Name", Color.rgb(0, 0, 0), Typeface.BOLD);
		tLastName = textFieldMaker.createTextField("Last Name");
		tLastName.setSingleLine(true);
		tLastName.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS | InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
		tLastName.setFilters(filter);
		textFields.put("lastname", tLastName);
		LayoutParams ibparams = new LayoutParams((int)(screenWidth * 0.32), (int)(screenWidth * 0.32));
		ibparams.gravity = Gravity.CENTER;
		LayoutParams lparams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lparams.gravity = Gravity.CENTER;
		ibUploadPhoto.setLayoutParams(ibparams);
		lUploadPhoto.setLayoutParams(lparams);
		lBirthday = labelMaker.createLabel("Birthday", Color.rgb(0, 0, 0), Typeface.BOLD);
		monthsMap = new LinkedHashMap<String, String>();
		monthsMap.put("Jan", "01");
		monthsMap.put("Feb", "02");
		monthsMap.put("Mar", "03");
		monthsMap.put("Apr", "04");
		monthsMap.put("May", "05");
		monthsMap.put("Jun", "06");
		monthsMap.put("Jul", "07");
		monthsMap.put("Aug", "08");
		monthsMap.put("Sep", "09");
		monthsMap.put("Oct", "10");
		monthsMap.put("Nov", "11");
		monthsMap.put("Dec", "12");
		months = new ArrayList<String>(monthsMap.keySet());
		ddMonth = dropdownMaker.createDropDown(months, LinearLayout.LayoutParams.WRAP_CONTENT);
		days = new ArrayList<String>();
		for(int i = 1; i <= 31; i++) {
			days.add((i < 10) ? "0" + i : String.valueOf(i));
		}
		ddDay = dropdownMaker.createDropDown(days, LinearLayout.LayoutParams.WRAP_CONTENT);
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		years = new ArrayList<String>();
		for(int i = year - 16; i >= year - 99; i--) {
			years.add(String.valueOf(i));
		}
		ddYear = dropdownMaker.createDropDown(years, LinearLayout.LayoutParams.WRAP_CONTENT);
		contentLayout.addView(ibUploadPhoto);
		contentLayout.addView(lUploadPhoto);
		contentLayout.addView(lFirstName);
		contentLayout.addView(tFirstName);
		contentLayout.addView(lLastName);
		contentLayout.addView(tLastName);
		contentLayout.addView(lBirthday);
		bdayHolder = new LinearLayout(this);
		LayoutParams bdayHolderParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		bdayHolder.setLayoutParams(bdayHolderParams);
		bdayHolder.addView(ddMonth);
		bdayHolder.addView(ddDay);
		bdayHolder.addView(ddYear);
		contentLayout.addView(bdayHolder);
		lMobileNumber = labelMaker.createLabel("Mobile Number", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lMobileNumber);
		LinearLayout mobileNoLayout = new LinearLayout(this);
		contentLayout.addView(mobileNoLayout);
		lMobileNumberPrefix = labelMaker.createLabel("+63 ", Color.rgb(0, 0, 0), Typeface.NORMAL);
		mobileNoLayout.addView(lMobileNumberPrefix);
		tMobileNumber = textFieldMaker.createTextField("Mobile Number", 0, 1);
		tMobileNumber.setInputType(InputType.TYPE_CLASS_NUMBER);
		textFields.put("mobileno", tMobileNumber);
		mobileNoLayout.addView(tMobileNumber);
		lEmail = labelMaker.createLabel("E-mail", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lEmail);
		tEmail = textFieldMaker.createTextField("E-mail");
		textFields.put("email", tEmail);
		contentLayout.addView(tEmail);
		if(!fromFacebook) {
			lPassword = labelMaker.createLabel("Password", Color.rgb(0, 0, 0), Typeface.BOLD);
			contentLayout.addView(lPassword);
			tPassword = textFieldMaker.createTextField("Password");
			tPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			textFields.put("password", tPassword);
			contentLayout.addView(tPassword);
			lConfirmPassword = labelMaker.createLabel("Confirm Password", Color.rgb(0, 0, 0), Typeface.BOLD);
			contentLayout.addView(lConfirmPassword);
			tConfirmPassword = textFieldMaker.createTextField("Confirm Password");
			tConfirmPassword.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
			textFields.put("confirmPassword", tConfirmPassword);
			contentLayout.addView(tConfirmPassword);
		}
		cb = new CheckBox(this);
		cb.setText("By signing up, I agree to the");
		cb.setTextColor(Color.BLACK);
		shape = new GradientDrawable();
		shape.setSize((int)(screenWidth * 0.04), (int)(screenWidth * 0.04));
		shape.setStroke(2, Color.BLACK);
		cb.setButtonDrawable(shape);
		cb.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				if(isChecked) {
					shape.setColor(Color.BLACK);
				}
				else {
					shape.setColor(Color.WHITE);
				}
			}
		});
		LinearLayout agreementLayout = new LinearLayout(this);
		LayoutParams agreementParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		agreementLayout.setLayoutParams(agreementParams);
		agreementLayout.addView(cb);
		lTermsAndConditions = labelMaker.createLabel(" Terms & Conditions", Color.rgb(0, 0, 255), -1);
		lTermsAndConditions.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				RegistrationScreen.this.startActivity(new Intent(RegistrationScreen.this, TermsAndConditionsScreen.class));
			}
		}, lTermsAndConditions, popup));
		agreementLayout.addView(lTermsAndConditions);
		LinearLayout agreementLayout1 = new LinearLayout(this);
		LayoutParams agreementParams1 = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		agreementParams1.gravity = Gravity.CENTER;
		agreementLayout1.setLayoutParams(agreementParams1);
		contentLayout.addView(agreementLayout);
		lAnd = labelMaker.createLabel("&", Color.BLACK, -1);
		agreementLayout1.addView(lAnd);
		lPrivacyPolicy = labelMaker.createLabel(" Privacy Policy", Color.rgb(0, 0, 255), -1);
		lPrivacyPolicy.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				RegistrationScreen.this.startActivity(new Intent(RegistrationScreen.this, PrivacyPolicyScreen.class));
			}
		}, lPrivacyPolicy, popup));
		agreementLayout1.addView(lPrivacyPolicy);
		contentLayout.addView(agreementLayout1);
		LinearLayout buttonLayout = new LinearLayout(this);
		LayoutParams buttonLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		buttonLayout.setLayoutParams(buttonLayoutParams);
		buttonLayout.setPadding(50, 10, 50, 10);
		bSignUp = buttonMaker.createButton("Sign Up", Color.rgb(252, 179, 24), (float)0.4, Gravity.LEFT);
		bSignUp.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				ArrayList<TextView> tempList = new ArrayList<TextView>(textFields.values());
				String popupMsg = null;
				if(InputChecker.isAllBlanks(tempList.toArray(new TextView[tempList.size()]))) {
					popupMsg = "Please don't leave any text fields blank.";
				}
				else {
					for(String key : textFields.keySet()) {
						if((popupMsg = InputChecker.checkForError(textFields.get(key), key)) != null) {
							break;
						}
					}
					if(popupMsg == null && textFields.get("confirmPassword") != null) {
						popupMsg = InputChecker.checkForError(textFields.get("password"), textFields.get("confirmPassword"), "confirmPassword");
					}
					if(popupMsg == null && !cb.isChecked()) {
						popupMsg = "Please indicate that you agree to the Terms and Conditions & Privacy Policy.";
					}
					if(popupMsg == null) {
						textFields.remove("confirmPassword");
						ArrayList<String> keyList = new ArrayList<String>(textFields.keySet());
						ArrayList<String> valuesList = new ArrayList<String>();
						for(String key : keyList) {
							valuesList.add(textFields.get(key).getText().toString());
						}
						keyList.add("birthday");
						valuesList.add(ddYear.getSelectedItem().toString() + "-" +
							monthsMap.get(ddMonth.getSelectedItem().toString()) + "-" +
							ddDay.getSelectedItem().toString());
						if(fromFacebook) {
							keyList.add("facebookid");
							try {
								valuesList.add(fbDetails.getString("id"));
							}
							catch(Exception e) {
								e.printStackTrace();
							}
						}
						/* if(imageUri != null) {
							keyList.add("photo");
							valuesList.add(imageUri.toString());
						} */
						String[][] fields = new String[2][];
						fields[0] = keyList.toArray(new String[keyList.size()]);
						fields[1] = valuesList.toArray(new String[valuesList.size()]);
						new PostTask(RegistrationScreen.this, TranseatsIP.IP + "/register", new AccountsProcessor(RegistrationScreen.this, contentLayout, null), contentLayout).execute(fields);
					}
				}
				if(popupMsg != null) {
					popupwindowmaker.showPopupWindow(popupMsg, contentLayout);
				}
			}
		}, bSignUp, popup));
		buttonLayout.addView(bSignUp);
		LayoutParams bSignUpLP = (LayoutParams)bSignUp.getLayoutParams();
		bSignUpLP.setMargins(0, 0, 25, 0);
		bCancel = buttonMaker.createButton("Cancel", Color.rgb(195, 195, 195), (float)0.4, Gravity.RIGHT);
		buttonLayout.addView(bCancel);
		LayoutParams bCancelLP = (LayoutParams)bCancel.getLayoutParams();
		bCancelLP.setMargins(25, 0, 0, 0);
		bCancel.setOnClickListener(new OnClickListenerProxy(new View.OnClickListener() {
			public void onClick(View v) {
				RegistrationScreen.this.finish();
			}
		}, bCancel, popup));
		contentLayout.addView(buttonLayout);
		scrollView.addView(contentLayout);
		headerMaker = new TranseatsHeaderMaker(this, "REGISTRATION", false);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(scrollView);
		setContentView(mainLayout);
		if(fromFacebook) {
			fillTextFields();
		}
	}

	@Override
	public void onActivityResult (int requestCode, int resultCode, Intent data) {
		if(requestCode == 5) {
			if(resultCode == Activity.RESULT_OK) {
				if(data != null && data.getData() != null) {
					imageUri = data.getData();
				}
				try {
					if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) !=
						PackageManager.PERMISSION_GRANTED) {
						ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
							6);
					}
					else {
						ibUploadPhoto.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri));
					}
				}
				catch(Exception e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		try {
			if(requestCode == 6) {
				if (grantResults.length > 0
					&& grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					ibUploadPhoto.setImageBitmap(MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri));
				}
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void openChooser() {
		final List<Intent> allIntents = new ArrayList<Intent>();
		imageUri = Uri.fromFile(new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "temp.jpg"));
		final Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		captureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
		allIntents.add(captureIntent);
		final Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT);
		galleryIntent.setType("image/*");
		final Intent chooserIntent = Intent.createChooser(galleryIntent, "Choose an action");
		chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, allIntents.toArray(new Parcelable[]{}));
		startActivityForResult(chooserIntent, 5);
	}

	public void fillTextFields() {
		try {
			fbDetails = new JSONObject(this.getIntent().getStringExtra("facebookDetails"));
			if(fbDetails.has("first_name")){
				tFirstName.setText(fbDetails.getString("first_name"));
			}
			if(fbDetails.has("last_name")){
				tLastName.setText(fbDetails.getString("last_name"));
			}
			if(fbDetails.has("email")){
				tEmail.setText(fbDetails.getString("email"));
			}
			if(fbDetails.has("birthday")){
				String[] date = fbDetails.getString("birthday").split("/");
				Calendar calendar = Calendar.getInstance();
				ddMonth.setSelection(Integer.parseInt(date[0]) - 1);
				ddDay.setSelection(Integer.parseInt(date[1]) - 1);
				ddYear.setSelection(calendar.get(Calendar.YEAR) - Integer.parseInt(date[2]) - 16);
			}
			if(fbDetails.has("picture")){
				new SetAsFBPhotoTask(ibUploadPhoto).execute(fbDetails.getJSONObject("picture").getJSONObject("data").getString("url"));
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
