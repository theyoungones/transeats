package com.transeats;

import android.view.View;
import android.widget.TextView;
import android.widget.Spinner;

public class BlankInputChecker
{
	public static boolean check(TextView... views) {
		for(TextView view : views) {
			if("".equals(view.getText().toString())) {
				return(false);
			}
		}
		return(true);
	}

	public static boolean checkDropdowns(Spinner... dropdowns) {
		for(Spinner dropdown : dropdowns) {
			if(dropdown.getCount() == 0 || "".equals(dropdown.getSelectedItem().toString())) {
				return(false);
			}
		}
		return(true);
	}
}
