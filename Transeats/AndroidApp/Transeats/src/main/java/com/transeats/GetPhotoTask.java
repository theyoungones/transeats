package com.transeats;

import java.net.HttpURLConnection;
import android.app.Activity;
import java.net.URL;
import android.os.AsyncTask;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import android.util.JsonWriter;
import android.widget.TextView;
import android.widget.PopupWindow;
import android.content.Context;
import android.widget.LinearLayout;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.content.Intent;

public class GetPhotoTask extends AsyncTask<String, Void, String>
{
	LinearLayout layout;
	TranseatsPopupWindowMaker popupwindowmaker;
	Context context;
	URL url;

	public GetPhotoTask(String u, TranseatsPopupWindowMaker pwm, LinearLayout cl, Context c) {
		try {
			url = new URL(u);
			System.out.println(url.toString());
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		layout = cl;
		popupwindowmaker = pwm;
		context = c;
	}

	protected String doInBackground(String... str) {
		try {
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			if(urlConnection.getResponseCode() == 200) {
				InputStream inputStream = urlConnection.getInputStream();
				Bitmap img = BitmapFactory.decodeStream(inputStream);
				FileOutputStream outputStream = context.openFileOutput(str[0], Context.MODE_PRIVATE);
				System.out.println("File write: " + img.compress(Bitmap.CompressFormat.JPEG, 100, outputStream));
				inputStream.close();
				outputStream.close();
			}
			urlConnection.disconnect();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(str[0]);
	}

	protected void onPostExecute(String result) {
		try {
			context.startActivity(new Intent(context, FindTrips.class));
			((Activity)context).finish();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
