package com.transeats;

import android.content.Context;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteDatabase;

public class TranseatsSQLiteHelper extends SQLiteOpenHelper
{
	public static final String TABLE_NAME = "account";
	private static final String DATABASE_NAME = "account.db";
	private static final int DATABASE_VERSION = 1;
	private static final String DATABASE_CREATE = "CREATE TABLE IF NOT EXISTS account(" +
		"firstname TEXT NOT NULL, lastname TEXT NOT NULL, " +
		"birthday TEXT NOT NULL, mobileno INTEGER NOT NULL," +
		"email TEXT NOT NULL, password TEXT, " +
		"facebookid TEXT, photo TEXT);";
	private static TranseatsSQLiteHelper instance = null;

	private TranseatsSQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	public static TranseatsSQLiteHelper getInstance(Context context) {
		if(instance == null) {
			instance = new TranseatsSQLiteHelper(context);
		}
		return(instance);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		System.out.println("Upgrading database from version " + oldVersion + " to " + newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS account");
		onCreate(db);
	}
}
