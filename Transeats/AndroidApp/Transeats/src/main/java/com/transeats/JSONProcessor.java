package com.transeats;

import android.view.View;
import java.util.List;

public interface JSONProcessor
{
	public void process(Object res, View view, List<?> obj);
}
