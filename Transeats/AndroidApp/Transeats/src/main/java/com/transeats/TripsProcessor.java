package com.transeats;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONArray;
import android.view.View;
import android.content.Context;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ArrayAdapter;
import java.time.LocalDate;
import java.time.LocalTime;

public class TripsProcessor implements JSONProcessor
{
	Context con = null;

	public TripsProcessor(Context con) {
		this.con = con;
	}

	public void process(Object res, View view, List<?> obj) {
		try {
			if(res instanceof JSONArray) {
				JSONArray array = (JSONArray)res;
				android.widget.Toast.makeText(con, ((array.length() > 0) ? "A total of " + array.length() + " trips" : "Nothing") + " was found.", android.widget.Toast.LENGTH_SHORT).show();
				ArrayList<Trip> list = (ArrayList<Trip>)obj;
				for(int i = 0; i < array.length(); i++) {
					JSONObject jsonObj = array.getJSONObject(i);
					list.add(new Trip.Builder().id(jsonObj.getInt("tripid")).busNo(jsonObj.getString("busno")).driver(jsonObj.getString("driver")).conductor(jsonObj.getString("conductor")).route(jsonObj.getString("origin") + " - " + jsonObj.getString("destination")).origin(jsonObj.getString("origin")).destination(jsonObj.getString("destination")).date(jsonObj.getString("tripdate").split("T")[0]).departureTime(jsonObj.getString("departure")).duration(jsonObj.getInt("duration")).rate(jsonObj.getInt("rate")).capacity(jsonObj.getInt("capacity")).availableSeats(jsonObj.getInt("availableseats")).build());
				}
				ListView listview = (ListView)view;
				((TranseatsArrayAdapter)listview.getAdapter()).notifyDataSetChanged();
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
