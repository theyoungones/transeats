package com.transeats;

import android.app.Activity;
import android.widget.Button;
import android.view.View;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.view.Gravity;
import android.util.DisplayMetrics;
import android.graphics.Color;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.content.Context;
import com.transeats.R;

public class TranseatsPopupWindowMaker
{
	private Context con = null;
	private static PopupWindow popup = null;
	private static TextView tv = null;
	private static TranseatsPopupWindowMaker instance = null;

	private TranseatsPopupWindowMaker(Context c) {
		con = c;
		popup = createPopupWindow();
	}

	public static TranseatsPopupWindowMaker getInstance(Context context) {
		if(instance == null || (instance != null && instance.con != context)) {
			instance = new TranseatsPopupWindowMaker(context);
		}
		return(instance);
	}

	public PopupWindow createPopupWindow() {
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		final int screenHeight = displayMetrics.heightPixels;
		final int screenWidth = displayMetrics.widthPixels;
		final LinearLayout popupLayout = new LinearLayout(con);
		popupLayout.setOrientation(LinearLayout.VERTICAL);
		popupLayout.setPadding(20, 20, 20, 20);
		tv = new TextView(con);
		tv.setTextSize(20.0f);
		tv.setTextColor(Color.rgb(140,140,140));
		tv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1));
		tv.setGravity(Gravity.CENTER);
		popupLayout.addView(tv);
		final Button popupButton = new Button(con);
		popupButton.setText("Close");
		popupLayout.addView(popupButton);
		popupLayout.setBackgroundResource(R.drawable.backgroundborder);
		final PopupWindow popup = new PopupWindow(popupLayout, (int)(screenWidth * 0.6), (int)(screenHeight * 0.23));
		popupButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				popup.dismiss();
			}
		});
		return(popup);
	}

	public PopupWindow getPopup() {
		return(popup);
	}

	public PopupWindow showPopupWindow(String message, LinearLayout layout) {
		if(popup != null && !popup.isShowing()) {
			tv.setText(message);
			popup.showAtLocation(layout, Gravity.CENTER, 0, 0);
		}
		return(popup);
	}
}
