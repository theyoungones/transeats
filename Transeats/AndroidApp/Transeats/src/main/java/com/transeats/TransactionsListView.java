package com.transeats;

import android.widget.ListView;
import java.util.ArrayList;
import java.util.HashMap;
import android.content.Context;
import android.widget.SimpleAdapter;
import android.widget.AdapterView;
import android.view.View;

public class TransactionsListView extends ListView
{
	ArrayList<HashMap<String, String>> data;
	Context context;

	public TransactionsListView(Context ctx) {
		super(ctx);
		context = ctx;
		data = new ArrayList<HashMap<String, String>>();
		setContent();
	}

	public void addItemToList(String transactionID, String destination) {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("transactionID", transactionID);
		map.put("destination", destination);
		data.add(map);
	}

	public void setContent() {
		String[] from = {"transactionID", "destination"};
		int[] to = {android.R.id.text1, android.R.id.text2};
		SimpleAdapter sa = new SimpleAdapter(context, data, android.R.layout.simple_list_item_2, from, to);
		setAdapter(sa);
	}
}