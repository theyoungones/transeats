package com.transeats;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import java.util.ArrayList;
import java.util.List; 
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.graphics.Color;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;
import android.widget.Spinner;
import android.widget.Toast;
import android.widget.Button;
import android.graphics.drawable.Drawable;
import android.widget.ListView;
import android.graphics.Color;
import java.util.Calendar;
import org.json.JSONObject;
import android.widget.PopupWindow;
import android.widget.ScrollView;
import android.view.ViewGroup;
import java.time.LocalDate;

public class FindTrips extends Activity
{
	ScrollView scrollView = null;
	LinearLayout originHolder = null;
	LinearLayout destinationHolder = null;
	LinearLayout dateHolder = null;
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	LinearLayout line = null;
	LinearLayout iconSortParams = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsTextFieldMaker textFieldMaker = null;
	TranseatsLabelMaker labelMaker = null;
	TranseatsDropDownMaker dropdownMaker = null;
	TranseatsButtonMaker buttonMaker = null;
	TranseatsBoxMaker boxMaker = null;
	Button bFind = null;
	View vLine = null;
	TextView lDateOfTrip = null;
	TextView lFrom = null;
	TextView lTo = null;
	TextView lMonth = null;
	TextView lDay = null;
	TextView lYear = null;
	TextView lResult = null;
	TextView lSort = null;
	Spinner ddFrom = null;
	Spinner ddTo = null;
	Spinner ddMonth = null;
	Spinner ddDay = null;
	Spinner ddYear = null;
	List<String> originTerminal = null;
	List<String> destinationTerminal = null;
	List<String> months = null;
	List<String> days = null;
	List<String> years = null;
	ImageView ibSort = null;
	ArrayList<Trip> result = null;
	ListView lvResult = null;
	TranseatsPopupWindowMaker popupwindowmaker = null;
	String[] mmm = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
	AccountsDataSource datasource;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		textFieldMaker = new TranseatsTextFieldMaker(this);
		labelMaker = new TranseatsLabelMaker(this);
		dropdownMaker = new TranseatsDropDownMaker(this);
		boxMaker = new TranseatsBoxMaker(this);
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(this);
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout = new LinearLayout(this);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		LayoutParams p1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
		p1.gravity = Gravity.CENTER;
		contentLayout.setLayoutParams(p1);
		contentLayout.setPadding(30, 5, 30, 5);
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		lDateOfTrip = labelMaker.createLabel("Date of trip", Color.rgb(0, 0, 0), Typeface.BOLD);
		lFrom = labelMaker.createLabel("From   ", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(lDateOfTrip);
		originHolder = new LinearLayout(this);
		LocationsProcessor lprocessor = new LocationsProcessor(this);
		Calendar calendar = Calendar.getInstance();
		ddFrom = dropdownMaker.createDropDown();
		new GetTask(this, TranseatsIP.IP + "/terminals", ddFrom, lprocessor, popupwindowmaker, contentLayout).execute("", "");
		LinearLayout.LayoutParams originHolderParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
		originHolderParams.setMargins(0, 30, 0, 0);
		originHolder.setLayoutParams(originHolderParams);
		originHolder.addView(lFrom);
		originHolder.addView(ddFrom);
		contentLayout.addView(originHolder);
		lTo = labelMaker.createLabel("To        ", Color.rgb(0, 0, 0), -1);
		destinationHolder = new LinearLayout(this);
		ddTo = dropdownMaker.createDropDown();
		new GetTask(this, TranseatsIP.IP + "/terminals", ddTo, lprocessor, popupwindowmaker, contentLayout).execute("", "");
		LinearLayout.LayoutParams destinationHolderParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		destinationHolderParams.setMargins(0, 30, 0, 0);
		destinationHolder.setLayoutParams(destinationHolderParams);
		destinationHolder.addView(lTo);
		destinationHolder.addView(ddTo);
		contentLayout.addView(destinationHolder);
		lMonth = labelMaker.createLabel("Month  ", Color.rgb(0, 0, 0), -1);
		months = new ArrayList<String>();
		for(int i = 0; i < mmm.length; i++) {
			months.add(mmm[i]);
		}
		ddMonth = dropdownMaker.createDropDown(months, LinearLayout.LayoutParams.WRAP_CONTENT);
		ddMonth.setSelection(calendar.get(Calendar.MONTH));
		lDay = labelMaker.createLabel("Day  ", Color.rgb(0, 0, 0), -1);
		days = new ArrayList<String>();
		for(int i = 1; i <=31; i++) {
			days.add((i < 10) ? "0" + i : String.valueOf(i));
		}
		ddDay = dropdownMaker.createDropDown(days, LinearLayout.LayoutParams.WRAP_CONTENT);
		ddDay.setSelection(calendar.get(Calendar.DAY_OF_MONTH) - 1);
		lYear = labelMaker.createLabel("Year  ", Color.rgb(0, 0, 0), -1);
		int thisYear = calendar.get(Calendar.YEAR);
		years = new ArrayList<String>();
		for(int i = thisYear; i <= thisYear + 1; i++) {
			years.add(String.valueOf(i));
		}
		ddYear = dropdownMaker.createDropDown(years, LinearLayout.LayoutParams.WRAP_CONTENT);
		dateHolder = new LinearLayout(this);
		LinearLayout.LayoutParams dateHolderParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,LinearLayout.LayoutParams.WRAP_CONTENT);
		dateHolderParams.setMargins(0, 30, 0, 50);
		dateHolder.setLayoutParams(dateHolderParams);
		dateHolder.addView(lMonth);
		dateHolder.addView(ddMonth);
		dateHolder.addView(lDay);
		dateHolder.addView(ddDay);
		dateHolder.addView(lYear);
		dateHolder.addView(ddYear);
		contentLayout.addView(dateHolder);
		buttonMaker = new TranseatsButtonMaker(this);
		bFind = buttonMaker.createButton("Find", Color.rgb(252, 179, 24), (float)0.6, Gravity.CENTER);
		contentLayout.addView(bFind);
		LinearLayout line = boxMaker.createBox(LayoutParams.MATCH_PARENT, (int)(screenWidth * 0.01), Color.rgb(140, 140, 140), Color.rgb(140, 140, 140));
		contentLayout.addView(line);
		RelativeLayout resultHolder = new RelativeLayout(this);
		RelativeLayout.LayoutParams rHolderParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		resultHolder.setPadding(0, (int)(screenWidth * 0.05), 0, 50);
		resultHolder.setLayoutParams(rHolderParams);
		lResult = labelMaker.createLabel("Results:", Color.rgb(0, 0, 0), -1);
		resultHolder.addView(lResult);
		lSort = labelMaker.createLabel("Sort by time:", Color.rgb(0, 0, 0), -1);
		lSort.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.upbutton1, 0);
		RelativeLayout.LayoutParams lSortParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
		lSortParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
		lSort.setLayoutParams(lSortParams);
		resultHolder.addView(lSort);
		contentLayout.addView(resultHolder);
		lvResult = new ListView(this);
		LinearLayout.LayoutParams lvResultParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
		result = new ArrayList<Trip>();
		TranseatsDataHolder.trips = result;
		lvResult.setLayoutParams(lvResultParams);
		TranseatsArrayAdapter adapter = new TranseatsArrayAdapter(this, result);
		lvResult.setAdapter(adapter);
		lvResult.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				TranseatsDataHolder.trip = result.get(position);
				startActivity(new Intent(FindTrips.this, TripScreen.class));
			}
		});
		contentLayout.addView(lvResult);
		bFind.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(BlankInputChecker.checkDropdowns(ddFrom, ddTo) == false) {
					popupwindowmaker.showPopupWindow("Please don't leave any text fields blank.", contentLayout);
				}
				else {
					String dd = ddYear.getSelectedItem().toString() + "-" + (ddMonth.getSelectedItemPosition() + 1) + "-" + ddDay.getSelectedItem().toString();
					String qs = "?origin=" + ddFrom.getSelectedItem().toString() + "&destination=" + ddTo.getSelectedItem().toString() + "&date=" + dd;
					((ArrayAdapter<String>)lvResult.getAdapter()).clear();
					new GetTask(getApplicationContext(), TranseatsIP.IP + "/trips" + qs.replaceAll(" ", "_"), lvResult, new TripsProcessor(getApplicationContext()), popupwindowmaker, contentLayout, result).execute("", "");
				}
			}
		});
		datasource = AccountsDataSource.getInstance(this);
		System.out.println("Find Trips");
		datasource.open();
		String[][] account = datasource.getAccount();
		headerMaker = new TranseatsHeaderMaker(this, "FIND TRIPS", account);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(contentLayout);
		setContentView(mainLayout);
	}

	/* @Override
	public void onStop() {
		super.onStop();
		datasource.deleteAccounts();
		System.out.println("onStop Activity");
	} */
}
