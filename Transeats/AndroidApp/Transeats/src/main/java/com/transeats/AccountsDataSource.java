package com.transeats;

import android.database.sqlite.SQLiteDatabase;
import android.database.SQLException;
import android.database.Cursor;
import android.content.Context;
import android.content.ContentValues;

public class AccountsDataSource
{
	private SQLiteDatabase database;
	private TranseatsSQLiteHelper dbHelper;
	private static AccountsDataSource instance = null;

	private AccountsDataSource(Context context) {
		dbHelper = TranseatsSQLiteHelper.getInstance(context);
	}

	public static AccountsDataSource getInstance(Context context) {
		if(instance == null) {
			instance = new AccountsDataSource(context);
		}
		return(instance);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public void createAccount(String[][] account) {
		ContentValues values = new ContentValues();
		for(int i = 0; i < account.length; i++) {
			if(account[i][0] != null) {
				values.put(account[i][0], account[i][1]);
				System.out.println(account[i][0] + account[i][1]);
			}
		}
		try {
			long insertId = database.insertOrThrow("account", null, values);
			System.out.println("ID: " + insertId);
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public String[][] getAccount() {
		Cursor cursor = database.query("account", null, null, null,
			null, null, null);
		cursor.moveToFirst();
		String[][] account = null;
		if(cursor.getCount() > 0) {
			String[] columnNames = cursor.getColumnNames();
			account = new String[columnNames.length][2];
			for(int col = 0, index = 0; col < columnNames.length; col++) {
				account[index][0] = columnNames[col];
				if(cursor.getType(col) == Cursor.FIELD_TYPE_STRING) {
					account[index][1] = cursor.getString(col);
				}
				else if(cursor.getType(col) == Cursor.FIELD_TYPE_INTEGER) {
					account[index][1] = Integer.toString(cursor.getInt(col));
				}
				if(account[index][1] == null || account[index][1].equals("")) {
					continue;
				}
				System.out.println(account[index][0] + ": " + account[index][1]);
				index++;
			}
		}
		cursor.close();
		return(account);
	}

	public void deleteAccounts() {
		System.out.println("Rows deleted: " + database.delete("account", "1", null));
	}
}
