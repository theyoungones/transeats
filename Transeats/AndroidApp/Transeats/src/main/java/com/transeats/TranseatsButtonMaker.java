package com.transeats;

import android.graphics.Color;
import android.content.Context;
import android.graphics.Typeface;
import java.util.List;
import android.util.DisplayMetrics;
import android.graphics.drawable.GradientDrawable;
import android.app.Activity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;

public class TranseatsButtonMaker
{
	GradientDrawable shape = null;
	Context con = null;
	int screenWidth = 0;
	int screenHeight = 0;

	public TranseatsButtonMaker(Context con) {
		this.con = con;
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
	}

	public Button createButton(String label, int color, float width, int gravity) {
		Button button = new Button(con);
		LayoutParams params = new LayoutParams((int)(screenWidth * width), (int)(screenHeight * 0.06));
		if(gravity > -1) {
			params.gravity = gravity;
		}
		params.setMargins(0, (int)(screenHeight * 0.01), 0, (int)(screenHeight * 0.01));
		button.setLayoutParams(params);
		button.setBackgroundColor(color);
		button.setTextColor(Color.WHITE);
		button.setText(label);
		button.setTextSize(14);
		shape = new GradientDrawable();
		shape.setCornerRadius(20);
		shape.setStroke(2, Color.rgb(140, 140, 140));
		shape.setColor(color);
		button.setBackground(shape);
		return(button);
	}

	public Button createButtonForRelativeLayout(String label, int color, float width, int align) {
		Button button = new Button(con);
		RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams((int)(screenWidth * width), (int)(screenHeight * 0.06));
		if(align > -1) {
			params.addRule(align);
		}
		button.setLayoutParams(params);
		button.setBackgroundColor(color);
		button.setText(label);
		button.setTextColor(Color.WHITE);
		button.setTextSize(14);
		shape = new GradientDrawable();
		shape.setCornerRadius(20);
		shape.setStroke(2, Color.rgb(140, 140, 140));
		shape.setColor(color);
		button.setBackground(shape);
		return(button);
	}
}
