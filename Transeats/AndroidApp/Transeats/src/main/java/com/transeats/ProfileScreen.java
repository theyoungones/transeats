package com.transeats;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.CheckBox;
import android.widget.Button;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import android.graphics.drawable.Drawable;
import android.view.View;
import org.json.JSONObject;
import android.widget.PopupWindow;
import android.widget.LinearLayout.LayoutParams;
import android.content.Intent;
import android.widget.ScrollView;

public class ProfileScreen extends Activity
{
	ScrollView scrollView = null;
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsTextFieldMaker textFieldMaker = null;
	TranseatsLabelMaker labelMaker = null;
	TranseatsDropDownMaker dropdownMaker = null;
	TranseatsButtonMaker buttonMaker = null;
	TranseatsPopupWindowMaker popupwindowmaker = null;
	ImageView iProfilePhoto = null;
	TextView lUploadPhoto = null;
	TextView lFirstName = null;
	TextView lUserFirstName = null;
	TextView lLastName = null;
	TextView lUserLastName = null;
	TextView lBirthday = null;
	TextView lUserBirthday = null;
	TextView lMobileNumber = null;
	TextView lUserMobileNumber = null;
	TextView lEmail = null;
	TextView lUserEmail = null;
	Button bEdit = null;
	AccountsDataSource datasource;
	LinkedHashMap<String, String> monthsMap = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		textFieldMaker = new TranseatsTextFieldMaker(this);
		labelMaker = new TranseatsLabelMaker(this);
		dropdownMaker = new TranseatsDropDownMaker(this);
		buttonMaker = new TranseatsButtonMaker(this);
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(this);
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout = new LinearLayout(this);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		mainLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		LayoutParams p1 = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
		p1.gravity = Gravity.CENTER;
		contentLayout.setLayoutParams(p1);
		contentLayout.setPadding(30, 5, 30, 5);
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		iProfilePhoto = new ImageView(this);
		iProfilePhoto.setImageResource(R.drawable.person);
		lUploadPhoto = new TextView(this);
		lUploadPhoto.setText("Upload Photo");
		lUploadPhoto.setTextColor(Color.rgb(0, 0, 0));
		lUploadPhoto.setTypeface(null, Typeface.BOLD);
		LayoutParams ibparams = new LayoutParams((int)(screenWidth * 0.32), (int)(screenWidth * 0.32));
		ibparams.gravity = Gravity.CENTER;
		LayoutParams lparams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		lparams.gravity = Gravity.CENTER;
		iProfilePhoto.setLayoutParams(ibparams);
		lUploadPhoto.setLayoutParams(lparams);
		contentLayout.addView(iProfilePhoto);
		contentLayout.addView(lUploadPhoto);
		lFirstName = labelMaker.createLabel("First Name", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lFirstName);
		lUserFirstName = labelMaker.createLabel("Juan", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(lUserFirstName);
		lLastName = labelMaker.createLabel("Last Name", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lLastName);
		lUserLastName = labelMaker.createLabel("Dela Cruz", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(lUserLastName);
		lBirthday = labelMaker.createLabel("Birthday", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lBirthday);
		lUserBirthday = labelMaker.createLabel("Jan 1, 1990", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(lUserBirthday);
		lMobileNumber = labelMaker.createLabel("Mobile Number", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lMobileNumber);
		lUserMobileNumber = labelMaker.createLabel("+639162341234", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(lUserMobileNumber);
		lEmail = labelMaker.createLabel("E-mail", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lEmail);
		lUserEmail = labelMaker.createLabel("juan.delacruz@gmail.com", Color.rgb(0, 0, 0), -1);
		contentLayout.addView(lUserEmail);
		bEdit = buttonMaker.createButton("Edit", Color.rgb(140, 140, 140), (float)0.6, Gravity.CENTER);
		bEdit.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				startActivity(new Intent(getApplicationContext(), EditProfileScreen.class));
			}
		});
		contentLayout.addView(bEdit);
		datasource = AccountsDataSource.getInstance(this);
		System.out.println("Profile");
		datasource.open();
		String[][] account = datasource.getAccount();
		headerMaker = new TranseatsHeaderMaker(this, "PROFILE", account);
		scrollView.addView(contentLayout);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(scrollView);
		setContentView(mainLayout);
		monthsMap = new LinkedHashMap<String, String>();
		monthsMap.put("01", "Jan");
		monthsMap.put("02", "Feb");
		monthsMap.put("03", "Mar");
		monthsMap.put("04", "Apr");
		monthsMap.put("05", "May");
		monthsMap.put("06", "Jun");
		monthsMap.put("07", "Jul");
		monthsMap.put("08", "Aug");
		monthsMap.put("09", "Sep");
		monthsMap.put("10", "Oct");
		monthsMap.put("11", "Nov");
		monthsMap.put("12", "Dec");
		setLabelText(account);
	}

	public void setLabelText(String[][] account) {
		for(String[] row : account ) {
			if(row.length == 2 && row[0] != null) {
				switch(row[0]) {
					case "firstname":
						lUserFirstName.setText(row[1]);
						break;
					case "lastname":
						lUserLastName.setText(row[1]);
						break;
					case "birthday":
						String birthday = "";
						Pattern p = Pattern.compile("(\\d{4})-(\\d{2})-(\\d{2})");
						Matcher m = p.matcher(row[1]);
						while(m.find()) {
							birthday += monthsMap.get(m.group(2)) + " " + m.group(3) + ", " + m.group(1);
						}
						lUserBirthday.setText(birthday);
					case "mobileno":
						lUserMobileNumber.setText(row[1]);
						break;
					case "email":
						lUserEmail.setText(row[1]);
						break;
					default:
						break;
				}
			}
		}
	}
}
