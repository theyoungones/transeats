package com.transeats;

import android.app.Activity;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.content.Context;
import android.content.Intent;
import android.widget.PopupWindow;
import org.json.JSONObject;
import org.json.JSONArray;
import java.util.List;

public class AccountsProcessor implements JSONProcessor
{
	Context con = null;
	TranseatsPopupWindowMaker popupwindowmaker;
	LinearLayout layout;
	JSONObject fbDetails = null;
	String task;
	String photo = null;

	public AccountsProcessor(Context con, LinearLayout cl, String fbDetails) {
		this.con = con;
		layout = cl;
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(con);
		try {
			if(fbDetails != null) {
				this.fbDetails = new JSONObject(fbDetails);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}

	public void setTask(String t) {
		task = t;
	}

	public void process(Object res, View view, List<?> obj) {
		System.out.println(res.getClass().getName());
		System.out.println("Accounts Processor: Process");
		if(res instanceof JSONArray) {
			JSONArray array = (JSONArray)res;
			if(fbDetails != null) {
				doFacebook(array);
			}
			else {
				System.out.println("Task: " + task);
				if("signin".equals(task)) {
					doSignIn(array);
				}
				else if("registration".equals(task)) {
					doRegistration(array);
				}
				else {
					doForgotPassword(array);
				}
			}
		}
	}

	public void doFacebook(JSONArray array) {
		System.out.println("Method doFacebook");
		if(array.length() > 0) {
			doSignIn(array);
		}
		else {
			Intent intent = new Intent(con, RegistrationScreen.class);
			intent.putExtra("facebookDetails", fbDetails.toString());
			con.startActivity(intent);
		}
	}

	public void doRegistration(final JSONArray array) {
		System.out.println("Method doRegistration");
		if(array.length() > 0) {
			PopupWindow popup = popupwindowmaker.showPopupWindow("Registration Successful", layout);
			popup.setOnDismissListener(new PopupWindow.OnDismissListener() {
				public void onDismiss() {
					doSignIn(array);
				}
			});
		}
	}

	public void doForgotPassword(JSONArray array) {
		System.out.println("Method doForgotPassword");
		String popupMsg = null;
		if(array.length() > 0) {
			popupMsg = "An e-mail with instructions on how to change your password has been sent to your email address.";
		}
		else {
			popupMsg = "We could not find an account for that email address.";
		}
		if(popupMsg != null) {
			popupwindowmaker.showPopupWindow(popupMsg, layout);
		}
	}

	public void doSignIn(JSONArray array) {
		System.out.println("Method doSignIn");
		if(array.length() > 0) {
			System.out.println(array.toString());
			String[][] account = prepareAccount(array);
			AccountsDataSource datasource = AccountsDataSource.getInstance(con);
			datasource.open();
			datasource.deleteAccounts();
			datasource.createAccount(account);
			if(photo != null && !photo.equals("")) {
				new GetPhotoTask(TranseatsIP.IP + "/uploads/" + photo, popupwindowmaker, layout, con).execute(photo);
			}
			else {
				con.startActivity(new Intent(con, FindTrips.class));
				((Activity)con).finish();
			}
		}
		else {
			popupwindowmaker.showPopupWindow("Incorrect email and/or password.", layout);
		}
	}

	public String[][] prepareAccount(JSONArray array) {
		String[][] account = null;
		try {
			JSONObject object = array.getJSONObject(0);
			JSONArray keys = object.names();
			account = new String[keys.length()][2];
			for(int keyIndex = 0, index = 0; keyIndex < keys.length(); keyIndex++) {
				String key = keys.getString(keyIndex);
				if(key.equals("accountid")) {
					continue;
				}
				account[index][0] = key;
				Object value = object.get(key);
				if(value instanceof String) {
					account[index][1] = (String)value;
				}
				else if(value instanceof Long) {
					account[index][1] = ((Long)value).toString();
				}
				else if(value instanceof Integer) {
					account[index][1] = ((Integer)value).toString();
				}
				if(key.equals("photo") && account[index][1] != null) {
					photo = account[index][1];
				}
				if(account[index][1] == null || account[index][1].equals("")) {
					continue;
				}
				index++;
			}
		}
		catch(Exception e) {
			e.printStackTrace();
			popupwindowmaker.showPopupWindow(e.toString(), layout);
		}
		return(account);
	}
}
