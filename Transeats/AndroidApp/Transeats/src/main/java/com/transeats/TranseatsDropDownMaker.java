package com.transeats;

import android.graphics.Color;
import android.content.Context;
import android.graphics.Typeface;
import java.util.List;
import java.util.ArrayList;
import android.util.DisplayMetrics;
import android.graphics.drawable.GradientDrawable;
import android.app.Activity;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.view.Gravity;

public class TranseatsDropDownMaker
{
	GradientDrawable textFieldShape = null;
	Context con = null;
	int screenWidth = 0;
	int screenHeight = 0;

	public TranseatsDropDownMaker(Context con) {
		this.con = con;
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
		textFieldShape = new GradientDrawable();
		textFieldShape.setCornerRadius(20);
		textFieldShape.setStroke(2, Color.rgb(140, 140, 140));
	}

	public Spinner createDropDown(List list) {
		Spinner dropdown = createDropDown();
		((ArrayAdapter<String>)dropdown.getAdapter()).addAll(list);
		return(dropdown);
	}

	public Spinner createDropDown(int width) {
		Spinner dropdown = createDropDown();
		dropdown.getLayoutParams().width = width;
		return(dropdown);
	}

	public Spinner createDropDown(List list, int width) {
		Spinner dropdown = createDropDown(list);
		dropdown.getLayoutParams().width = width;
		return(dropdown);
	}

	public Spinner createDropDown() {
		final Spinner dropdown = new Spinner(con);
		final LayoutParams dparams = new LayoutParams(LayoutParams.MATCH_PARENT, (int)(screenHeight * 0.05));
		dparams.setMargins(0, 0, (int)(screenWidth * 0.01), 0);
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(con, android.R.layout.simple_spinner_item, new ArrayList<String>());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		dropdown.setAdapter(adapter);
		dropdown.setLayoutParams(dparams);
		dropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
			public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
				parent.setBackground(textFieldShape);
				((TextView)view).setTextColor(Color.BLACK);
			}
			@Override
			public void onNothingSelected(AdapterView<?> av) {
			}
		});
		return(dropdown);
	}
}
