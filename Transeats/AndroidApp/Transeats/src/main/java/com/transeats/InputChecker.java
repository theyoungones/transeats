package com.transeats;

import android.view.View;
import android.widget.TextView;

public class InputChecker
{
	public static final String BLANK_FIRST_NAME = "Please enter your first name.";
	public static final String BLANK_LAST_NAME = "Please enter your last name.";
	public static final String BLANK_MOBILE_NO = "Please enter your mobile number.";
	public static final String INVALID_MOBILE_NO = "Please enter a valid mobile number.";
	public static final String INVALID_EMAIL = "Please enter a valid email address.";
	public static final String BLANK_PASSWORD = "Please enter your password.";
	public static final String INVALID_PASSWORD = "Password must contain at least 6 characters.";
	public static final String INCORRECT_CONFIRM_PASSWORD = "Password and Confirm Password did not match.";

	public static boolean isAllBlanks(TextView... views) {
		for(TextView view : views) {
			if(!"".equals(view.getText().toString())) {
				return(false);
			}
		}
		return(true);
	}

	public static String checkForError(TextView view, String type) {
		String val = view.getText().toString();
		System.out.println("Checking for: " + type);
		System.out.println("Value: " + val);
		switch(type) {
			case "firstname":
				if("".equals(val)) {
					return(BLANK_FIRST_NAME);
				}
				break;
			case "lastname":
				if("".equals(val)) {
					return(BLANK_LAST_NAME);
				}
				break;
			case "mobileno":
				if("".equals(val)) {
					return(BLANK_MOBILE_NO);
				}
				else if(!val.matches("\\d{10}")) {
					return(INVALID_MOBILE_NO);
				}
				break;
			case "email":
				if("".equals(val) || !val.matches("\\S+@\\S+\\.\\S+")) {
					return(INVALID_EMAIL);
				}
				break;
			case "password":
				if("".equals(val)) {
					return(BLANK_PASSWORD);
				}
				else if(val.length() < 6) {
					return(INVALID_PASSWORD);
				}
				break;
		}
		return(null);
	}

	public static String checkForError(TextView view1, TextView view2, String type) {
		String val1 = view1.getText().toString();
		String val2 = view2.getText().toString();
		if("confirmPassword".equals(type)) {
			if(!val1.equals(val2)) {
				return(INCORRECT_CONFIRM_PASSWORD);
			}
		}
		return(null);
	}
}
