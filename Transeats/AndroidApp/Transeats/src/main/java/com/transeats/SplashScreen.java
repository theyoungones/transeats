package com.transeats;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ImageView;
import android.graphics.Color;
import android.view.Window;
import android.view.WindowManager;
import android.view.Gravity;
import android.util.DisplayMetrics;
import android.widget.RelativeLayout.LayoutParams;
import java.util.TimerTask;
import java.util.Timer;
import android.os.Handler;
import java.lang.Runnable;

public class SplashScreen extends Activity
{
	RelativeLayout contentLayout = null;
	ImageView iv = null;
	Activity thisAct = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		thisAct = (Activity)this;
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		contentLayout = new RelativeLayout(this);
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		iv = new ImageView(this);
		iv.setImageResource(R.drawable.transeats);
		LayoutParams p1 = new LayoutParams((int)(screenWidth * 0.8), (int)(screenWidth * 0.8));
		p1.addRule(RelativeLayout.CENTER_IN_PARENT);
		iv.setLayoutParams(p1);
		contentLayout.addView(iv);
		setContentView(contentLayout);
		final Handler handler = new Handler();
		final Timer timer = new Timer(false);
		TimerTask timerTask = new TimerTask() {
			boolean flag = false;
			@Override
			public void run() {
				handler.post(new Runnable() {
					@Override
					public void run() {
						if(flag == false) {
							iv.setImageResource(R.drawable.theyoungones);
							flag = true;
						}
						else {
							thisAct.startActivity(new Intent(getApplicationContext(), SignInScreen.class));
							timer.cancel();
							thisAct.finish();
						}
					}
				});
			}
		};
		timer.scheduleAtFixedRate(timerTask, 1500, 1000);
	}
}
