package com.transeats;

import java.util.ArrayList;
import java.util.List;
import org.json.JSONObject;
import org.json.JSONArray;
import android.widget.Spinner;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.content.Context;

public class LocationsProcessor implements JSONProcessor
{
	Context con = null;

	public LocationsProcessor(Context con) {
		this.con = con;
	}

	public void process(Object res, View view, List<?> obj) {
		try {
			if(res instanceof JSONArray) {
				JSONArray array = (JSONArray)res;
				List<String> list = new ArrayList<String>();
				for(int i = 0; i < array.length(); i++) {
					JSONObject jsonObj = array.getJSONObject(i);
					list.add(jsonObj.getString("locationname") + ", " + jsonObj.getString("city"));
				}
				Spinner dropdown = (Spinner)view;
				ArrayAdapter<String> adapter = (ArrayAdapter<String>)dropdown.getAdapter();
				adapter.addAll(list);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
