package com.transeats;

public class Trip
{
	public static class Builder {
		private int id;
		private String route;
		private String origin;
		private String destination;
		private String busNo;
		private String driver;
		private String conductor;
		private String date;
		private String departureTime;
		private int duration;
		private int rate;
		private int capacity;
		private int availableSeats;

		public Builder id(int id) {
			this.id = id;
			return(this);
		}

		public Builder route(String route) {
			this.route = route;
			return(this);
		}

		public Builder origin(String origin) {
			this.origin = origin;
			return(this);
		}

		public Builder destination(String destination) {
			this.destination = destination;
			return(this);
		}

		public Builder busNo(String busNo) {
			this.busNo = busNo;
			return(this);
		} 

		public Builder driver(String driver) {
			this.driver = driver;
			return(this);
		}

		public Builder conductor(String conductor) {
			this.conductor = conductor;
			return(this);
		}

		public Builder date(String date) {
			this.date = date;
			return(this);
		}

		public Builder departureTime(String departureTime) {
			this.departureTime = departureTime;
			return(this);
		}

		public Builder duration(int duration) {
			this.duration = duration;
			return(this);
		}

		public Builder rate(int rate) {
			this.rate = rate;
			return(this);
		}

		public Builder capacity(int capacity) {
			this.capacity = capacity;
			return(this);
		}

		public Builder availableSeats(int availableSeats) {
			this.availableSeats = availableSeats;
			return(this);
		}

		public int getId() {
			return(id);
		}

		public String getRoute() {
			return(route);
		}

		public String getOrigin() {
			return(origin);
		}

		public String getDestination() {
			return(destination);
		}

		public String getbusNo() {
			return(busNo);
		}

		public String getDriver() {
			return(driver);
		}

		public String getConductor() {
			return(conductor);
		}

		public String getDate() {
			return(date);
		}

		public String getDepartureTime() {
			return(departureTime);
		}

		public int getDuration() {
			return(duration);
		}

		public int getRate() {
			return(rate);
		}

		public int getCapacity() {
			return(capacity);
		}

		public int getAvailableSeats() {
			return(availableSeats);
		}

		public Trip build() {
			return(new Trip(this));
		}
	}

	final int ID;
	final String ROUTE;
	final String ORIGIN;
	final String DESTINATION;
	final String BUS_NO;
	final String DRIVER;
	final String CONDUCTOR;
	final String DATE;
	final String TIME;
	final int DURATION;
	final int RATE;
	final int CAPACITY;
	final int AVAILABLE_SEATS;

	public Trip(Builder builder) {
		ID = builder.getId();
		ROUTE = builder.getRoute();
		ORIGIN = builder.getOrigin();
		DESTINATION = builder.getDestination();
		BUS_NO = builder.getbusNo();
		DRIVER = builder.getDriver();
		CONDUCTOR = builder.getConductor();
		DATE = builder.getDate();
		TIME = builder.getDepartureTime();
		DURATION = builder.getDuration();
		RATE = builder.getRate();
		CAPACITY = builder.getCapacity();
		AVAILABLE_SEATS = builder.getAvailableSeats();
	}
}
