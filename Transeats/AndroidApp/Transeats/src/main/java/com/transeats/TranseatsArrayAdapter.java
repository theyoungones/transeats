package com.transeats;

import java.util.List;
import java.util.ArrayList;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.content.ClipData.Item;

public class TranseatsArrayAdapter extends ArrayAdapter<Trip>
{
	Context con = null;
	List<Trip> trips = null;

	public TranseatsArrayAdapter(Context context, int textViewResourceId) {
		super(context, textViewResourceId);
		con = context;
	}

	public TranseatsArrayAdapter(Context context, ArrayList<Trip> items) {
		super(context, 0, items);
		con = context;
		trips = items;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View item = convertView;
		if (item == null) {
			item = LayoutInflater.from(con).inflate(R.layout.trips_listview_item, parent, false);
		}
		Trip trip = trips.get(position);
		TextView route = (TextView) item.findViewById(R.id.trip_route);
		route.setText(trip.ROUTE);
		TextView tripid = (TextView) item.findViewById(R.id.trip_id);
		tripid.setText(String.valueOf(trip.ID));
		TextView availableseats = (TextView) item.findViewById(R.id.trip_availableseats);
		availableseats.setText(String.valueOf(trip.AVAILABLE_SEATS));
		TextView busno = (TextView) item.findViewById(R.id.trip_busno);
		busno.setText(trip.BUS_NO);
		TextView fare = (TextView) item.findViewById(R.id.trip_fare);
		fare.setText(String.valueOf(trip.RATE));
		TextView time = (TextView) item.findViewById(R.id.trip_time);
		time.setText(trip.TIME);
		return item;
	}
}
