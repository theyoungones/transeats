package com.transeats;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.view.View;
import org.json.JSONObject;
import android.widget.PopupWindow;
import android.widget.LinearLayout.LayoutParams;
import android.widget.ScrollView;
import android.content.Intent;

public class TranseatsChangePasswordScreen extends Activity
{
	ScrollView scrollView = null;
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsTextFieldMaker textFieldMaker = null;
	TranseatsLabelMaker labelMaker = null;
	TranseatsButtonMaker buttonMaker = null;
	TranseatsPopupWindowMaker popupwindowmaker = null;
	TextView lCurrentPassword = null;
	TextView lNewPassword = null;
	TextView lConfirmPassword = null;
	EditText tCurrentPassword = null;
	EditText tNewPassword = null;
	EditText tConfirmPassword = null;
	Button bSave = null;
	Button bCancel = null;
	PopupWindow popup = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		textFieldMaker = new TranseatsTextFieldMaker(this);
		labelMaker = new TranseatsLabelMaker(this);
		buttonMaker = new TranseatsButtonMaker(this);
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(this);
		popup = popupwindowmaker.getPopup();
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout = new LinearLayout(this);
		scrollView = new ScrollView(this);
		scrollView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
		contentLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		mainLayout.setBackgroundColor(Color.rgb(255, 255, 255));
		LayoutParams p1 = new LayoutParams(LayoutParams.MATCH_PARENT, 0, 1);
		p1.gravity = Gravity.CENTER;
		contentLayout.setLayoutParams(p1);
		contentLayout.setPadding(30, 5, 30, 5);
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		lCurrentPassword = labelMaker.createLabel("Current Password", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lCurrentPassword);
		tCurrentPassword = textFieldMaker.createTextField(null);
		contentLayout.addView(tCurrentPassword);
		lNewPassword = labelMaker.createLabel("New Password", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lNewPassword);
		tNewPassword = textFieldMaker.createTextField(null);
		contentLayout.addView(tNewPassword);
		lConfirmPassword = labelMaker.createLabel("Confirm Password", Color.rgb(0, 0, 0), Typeface.BOLD);
		contentLayout.addView(lConfirmPassword);
		tConfirmPassword = textFieldMaker.createTextField(null);
		contentLayout.addView(tConfirmPassword);
		LinearLayout buttonLayout = new LinearLayout(this);
		LayoutParams buttonLayoutParams = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		buttonLayout.setLayoutParams(buttonLayoutParams);
		buttonLayout.setPadding(50, 50, 50, 50);
		bSave = buttonMaker.createButton("Save", Color.rgb(252, 179, 24), (float)0.4, Gravity.LEFT);
		buttonLayout.addView(bSave);
		LayoutParams bSaveLP = (LayoutParams)bSave.getLayoutParams();
		bSaveLP.setMargins(0, 0, 25, 0);
		bCancel = buttonMaker.createButton("Cancel", Color.rgb(195, 195, 195), (float)0.4, Gravity.RIGHT);
		bSave.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				JSONObject obj = new JSONObject();
				String popupMsg = null;
				String[] fieldValues = {tCurrentPassword.getText().toString(), tNewPassword.getText().toString(),tConfirmPassword.getText().toString()};
				String confirmPassword = tConfirmPassword.getText().toString();
				if(fieldValues[0].equals("") && fieldValues[1].equals("") && confirmPassword.equals("")){
					popupMsg = "Please don't leave any text fields blank.";
				}
				else if(fieldValues[0].equals("")) {
					popupMsg = "Please enter your current password.";
				}
				else if(fieldValues[1].equals("")) {
					popupMsg = "Please enter your new password.";
				}
				else if(fieldValues[2].equals("")) {
					popupMsg = "Please enter your confirm password.";
				}
				else if(fieldValues[1].length() < 6) {
					popupMsg = "Password must contain at least 6 characters.";
				}
				else if(!fieldValues[1].equals(confirmPassword)) {
					popupMsg = "New password and Confirm password did not match.";
				}
				else {
					// new PostTask("http://192.168.1.6:8080/register", popupwindowmaker, popupLayout).execute(fieldValues);
				}
				if(popupMsg != null) {
					popupwindowmaker.showPopupWindow(popupMsg, contentLayout);
				}
			}
		});
		buttonLayout.addView(bCancel);
		LayoutParams bCancelLP = (LayoutParams)bCancel.getLayoutParams();
		bCancelLP.setMargins(25, 0, 0, 0);
		contentLayout.addView(buttonLayout);
		headerMaker = new TranseatsHeaderMaker(this, "CHANGE PASSWORD", false);
		scrollView.addView(contentLayout);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(scrollView);
		setContentView(mainLayout);
	}
}
