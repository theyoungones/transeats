package com.transeats;

import android.os.AsyncTask;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap;
import android.widget.ImageView;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.InputStream;

public class SetAsFBPhotoTask extends AsyncTask<String, Void, Bitmap>
{
	ImageView imageView;

	public SetAsFBPhotoTask(ImageView iv) {
		imageView = iv;
	}

	protected Bitmap doInBackground(String... str) {
		Bitmap img = null;
		try {
			URL url = new URL(str[0]);
			HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
			InputStream in = urlConnection.getInputStream();
			img = BitmapFactory.decodeStream(in);
			in.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(img);
	}

	protected void onPostExecute(Bitmap result) {
		imageView.setImageBitmap(result);
	}

}
