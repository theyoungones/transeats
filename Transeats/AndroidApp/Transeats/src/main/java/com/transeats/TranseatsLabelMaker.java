package com.transeats;

import android.widget.TextView;
import android.graphics.Color;
import android.content.Context;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;

public class TranseatsLabelMaker
{
	Context con = null;

	public TranseatsLabelMaker(Context con) {
		this.con = con;
	}

	public TextView createLabel(String label, int color, int style) {
		TextView text = new TextView(con);
		text.setText(label);
		text.setTextColor(color);
		if(style > 0) {
			text.setTypeface(null, style);
		}
		return(text);
	}

	public TextView createLabel(String label, int color, int style, int size) {
		TextView text = createLabel(label, color, style);
		text.setTextSize(size);
		return(text);
	}

	public TextView createLabelWithIcon(String label, int color, int style, Drawable d) {
		TextView text = new TextView(con);
		text.setText(label);
		text.setCompoundDrawables(d, null, null, null);
		text.setTextColor(color);
		if(style > 0) {
			text.setTypeface(null, style);
		}
		return(text);
	}
}
