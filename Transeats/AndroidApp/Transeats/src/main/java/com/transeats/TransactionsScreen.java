package com.transeats;

import android.app.Activity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.Window;
import android.graphics.Typeface;
import java.util.List;
import java.util.Calendar;
import java.util.ArrayList;
import android.graphics.drawable.Drawable;
import android.view.View;
import org.json.JSONObject;
import android.widget.PopupWindow;
import android.widget.LinearLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.graphics.drawable.GradientDrawable;
import android.widget.ListView;
import android.widget.ArrayAdapter;

public class TransactionsScreen extends Activity
{
	ListView lv = null;
	LinearLayout mainLayout = null;
	LinearLayout contentLayout = null;
	TranseatsHeaderMaker headerMaker = null;
	TranseatsPopupWindowMaker popupwindowmaker = null;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screenWidth = displayMetrics.widthPixels;
		int screenHeight = displayMetrics.heightPixels;
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(this);
		mainLayout = new LinearLayout(this);
		mainLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout = new LinearLayout(this);
		contentLayout.setOrientation(LinearLayout.VERTICAL);
		contentLayout.setBackgroundColor(Color.GRAY);
		LayoutParams p1 = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, 1);
		contentLayout.setLayoutParams(p1);
		contentLayout.setPadding((int)(screenWidth * 0.07), (int)(screenWidth * 0.05), (int)(screenWidth * 0.07), 0);
		TransactionsListView tlv = new TransactionsListView(this);
		tlv.addItemToList("Transaction ID: 20180801001", "Destination: Sta. Rosa - Cubao\nTime: 8:00 AM\nStatus: Pending");
		contentLayout.addView(tlv);
		headerMaker = new TranseatsHeaderMaker(this, "TRANSACTIONS", true);
		mainLayout.addView(headerMaker.getHeader());
		mainLayout.addView(contentLayout);
		setContentView(mainLayout);
	}
}
