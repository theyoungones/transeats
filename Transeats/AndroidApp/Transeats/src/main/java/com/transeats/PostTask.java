package com.transeats;

import android.content.Context;
import android.content.ContentResolver;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.util.JsonWriter;
import android.view.Gravity;
import android.view.ViewGroup;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Button;
import android.widget.PopupWindow;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.BufferedReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.BufferedOutputStream;
import java.io.DataOutputStream;
import java.lang.StringBuilder;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONObject;
import org.json.JSONArray;

public class PostTask extends AsyncTask<String[], Void, String>
{
	LinearLayout layout;
	TranseatsPopupWindowMaker popupwindowmaker;
	JSONProcessor jsonprocessor;
	Context context;
	URL url;
	PopupWindow popup = null;

	public PostTask(Context c, String u, JSONProcessor jsonprocessor, LinearLayout cl) {
		try {
			url = new URL(u);
			System.out.println("URL: " + url.toString());
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		layout = cl;
		popupwindowmaker = TranseatsPopupWindowMaker.getInstance(c);
		context = c;
		this.jsonprocessor = jsonprocessor;
	}

	protected String doInBackground(String[][] str) {
		StringBuilder sb = new StringBuilder("");
		try {
			if(ConnectionChecker.isConnected(context)) {
				HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
				urlConnection.setDoOutput(true);
				urlConnection.setChunkedStreamingMode(0);
				String photo = null;
				for(int i = 0; str.length == 2 && i < str[0].length ; i++) {
					System.out.println("Key: " + str[0][i]);
					System.out.println("Value: " + str[1][i]);
					if("photo".equals(str[0][i])) {
						if(str[1][i] != null && !"".equals(str[1][i])) {
							photo = str[1][i];
						}
						break;
					}
				}
				if(photo != null) {
					String lineEnd = "\r\n";
					String twoHyphens = "--";
					String boundary = "*****";
					int bytesRead, bytesAvailable, bufferSize;
					byte[] buffer;
					int maxBufferSize = 1024 * 1024;
					Uri uri = Uri.parse(photo);
					System.out.println("Photo URI: " + uri.toString());
					urlConnection.setRequestProperty("Connection", "Keep-Alive");
					urlConnection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
					DataOutputStream outputStream = new DataOutputStream(urlConnection.getOutputStream());
					outputStream.writeBytes(twoHyphens + boundary + lineEnd);
					for(int i = 0; i < str[0].length - 1; i++) {
						outputStream.writeBytes("Content-Disposition: form-data; name=\"" + str[0][i] + "\"" + lineEnd);
						outputStream.writeBytes(lineEnd);
						outputStream.writeBytes(str[1][i]);
						outputStream.writeBytes(lineEnd);
						outputStream.writeBytes(twoHyphens + boundary + lineEnd);
					}
					outputStream.writeBytes("Content-Disposition: form-data; name=\"file\";filename=\"" + uri.getLastPathSegment() + "\"" + lineEnd);
					outputStream.writeBytes(lineEnd);
					InputStream inputStream = context.getContentResolver().openInputStream(uri);
					bytesAvailable = inputStream.available();
					bufferSize = Math.min(bytesAvailable, maxBufferSize);
					buffer = new byte[bufferSize];
					bytesRead = inputStream.read(buffer, 0, bufferSize);
					while (bytesRead > 0) {
						outputStream.write(buffer, 0, bufferSize);
						bytesAvailable = inputStream.available();
						bufferSize = Math.min(bytesAvailable, maxBufferSize);
						bytesRead = inputStream.read(buffer, 0, bufferSize);
					}
					outputStream.writeBytes(lineEnd);
					outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
					inputStream.close();
					outputStream.flush();
					outputStream.close();
				}
				else {
					urlConnection.setRequestProperty("Content-Type", "application/json");
					OutputStream out = new BufferedOutputStream(urlConnection.getOutputStream());
					JsonWriter writer = new JsonWriter(new OutputStreamWriter(out));
					writer.beginObject();
					for(int i = 0; i < str[0].length; i++) {
						writer.name(str[0][i]).value(str[1][i]);
						System.out.println(str[0][i] + ": " + str[1][i]);
					}
					writer.endObject();
					writer.flush();
					writer.close();
				}
				InputStream in = urlConnection.getInputStream();
				BufferedReader br = new BufferedReader(new InputStreamReader(in));
				String line = "";
				while((line = br.readLine()) != null) {
					sb.append(line);
				}
				urlConnection.disconnect();
			}
			else {
				sb.append("{error : \"Please check your internet connection and try again.\"}");
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
		return(sb.toString());
	}

	protected void onPostExecute(String result) {
		try {
			System.out.println("Result: " + result);
			JSONObject obj = new JSONObject(result);
			if(obj.has("error")) {
				popupwindowmaker.showPopupWindow(obj.getString("error"), layout);
			}
			else {
				Object res = obj.get("result");
				if(obj.has("task") && jsonprocessor instanceof AccountsProcessor) {
					((AccountsProcessor)jsonprocessor).setTask(obj.getString("task"));
				}
				jsonprocessor.process(res, null, null);
			}
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
}
