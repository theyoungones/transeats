package com.transeats;

import android.graphics.drawable.GradientDrawable;
import android.graphics.Color;
import android.content.Context;
import android.app.Activity;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.RelativeLayout;
import android.util.DisplayMetrics;
import android.view.Gravity;

public class TranseatsBoxMaker
{
	Context con = null;
	int screenWidth = 0;
	int screenHeight = 0;

	public TranseatsBoxMaker(Context con) {
		this.con = con;
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
	}

	public LinearLayout createBox(int width, int height, int borderColor, int backgroundColor) {
		LinearLayout box = new LinearLayout(con);
		LayoutParams tParams = new LayoutParams(width, height);
		box.setLayoutParams(tParams);
		GradientDrawable shape = new GradientDrawable();
		shape.setStroke(3, borderColor);
		shape.setColor(backgroundColor);
		box.setBackground(shape);
		return(box);
	}

	public RelativeLayout createSeat(int width, int height, int borderColor, int backgroundColor, String text) {
		RelativeLayout box = new RelativeLayout(con);
		LayoutParams tParams = new LayoutParams(width, height);
		tParams.setMargins((int)(width * 0.15), (int)(width * 0.15), (int)(width * 0.15), (int)(width * 0.15));
		TextView tv = new TextView(con);
		LayoutParams tvParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		tvParams.addRule(RelativeLayout.CENTER_IN_PARENT);
		tvParams.addRule(RelativeLayout.CENTER_VERTICAL);
		tv.setLayoutParams(tvParams);
		tv.setTextColor(Color.BLACK);
		tv.setTextSize((int)(height * 0.15));
		tv.setText(text);
		box.setLayoutParams(tParams);
		GradientDrawable shape = new GradientDrawable();
		shape.setStroke(3, borderColor);
		shape.setColor(backgroundColor);
		box.setBackground(shape);
		box.addView(tv);
		return(box);
	}
}
