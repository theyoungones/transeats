package com.transeats;

import android.widget.EditText;
import android.graphics.drawable.GradientDrawable;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.content.Context;
import android.app.Activity;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class TranseatsTextFieldMaker
{
	GradientDrawable textFieldShape = null;
	Context con = null;
	int screenWidth = 0;
	int screenHeight = 0;

	public TranseatsTextFieldMaker(Context con) {
		this.con = con;
		DisplayMetrics displayMetrics = new DisplayMetrics();
		((Activity)con).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		screenWidth = displayMetrics.widthPixels;
		screenHeight = displayMetrics.heightPixels;
		textFieldShape = new GradientDrawable();
		textFieldShape.setCornerRadius(20);
		textFieldShape.setStroke(2, Color.rgb(140, 140, 140));
	}

	public EditText createTextField(String label) {
		EditText text = new EditText(con);
		LayoutParams tParams = new LayoutParams(LayoutParams.MATCH_PARENT, (int)(screenHeight * 0.06));
		tParams.setMargins(0, (int)(screenHeight * 0.005), 0, (int)(screenHeight * 0.005));
		text.setLayoutParams(tParams);
		if(label != null) {
			text.setHint(label);
			text.setHintTextColor(Color.rgb(140, 140, 140));
		}
		text.setTextColor(Color.rgb(0, 0, 0));
		text.setTextSize(14);
		text.setPadding(10, 0, 10, 0);
		text.setBackground(textFieldShape);
		return(text);
	}

	public EditText createTextField(String label, int width) {
		EditText text = createTextField(label);
		LayoutParams tParams = new LayoutParams(width, (int)(screenHeight * 0.06));
		tParams.setMargins(0, 0, 0, (int)(screenHeight * 0.01));
		text.setLayoutParams(tParams);
		return(text);
	}

	public EditText createTextField(String label, int width, int weight) {
		EditText text = createTextField(label);
		LayoutParams tParams = new LayoutParams(width, (int)(screenHeight * 0.06), weight);
		tParams.setMargins(0, 0, 0, (int)(screenHeight * 0.01));
		text.setLayoutParams(tParams);
		return(text);
	}
}
